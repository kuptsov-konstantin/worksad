﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
namespace KupcovKS.Logging
{
    public static class Logger
    {
        public static void Error(string message, string module)
        {
            WriteEntry(message, "error", module);
        }
        public static void Error(Exception ex, string module)
        {
            WriteEntry(ex.Message, "error", module);
        }
        public static void Warning(string message, string module)
        {
            WriteEntry(message, "warning", module);
        }
        public static void Info(string message, string module)
        {
            WriteEntry(message, "info", module);
        }
        private static void WriteEntry(string message, string type, string module)
        {
            Trace.WriteLine(string.Format("{0},{1},{2},{3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), type, module, message));
            Trace.Flush();        
        }
    }

    /// <summary>
    /// Лог
    /// </summary>
    public class CLog
    {
        #region Темы
        /// <summary>
        /// Без тем
        /// </summary>
        public static string file_def = "loging_withouttems.log";
        /// <summary>
        /// Сеть
        /// </summary>
        public static string file_net = "loging_net.log";
        /// <summary>
        /// Форма
        /// </summary>
        public static string file_form = "loging_form.log";
        /// <summary>
        /// Ошибки
        /// </summary>
        public static string file_error = "loging_error.log";
        /// <summary>
        /// Музыка
        /// </summary>
        public static string file_music = "loging_music.log";
        /// <summary>
        /// Папка
        /// </summary>
        public static string file_folder = "loging_folder.log";
        #endregion

        /// <summary>
        /// Запись в дефолтный лог файл.
        /// </summary>
        /// <param name="mes">Сообщение.</param>
        public CLog(string mes)
        {
            Log(file_def, mes);
        }
        /// <summary>
        /// Запись в лог
        /// </summary>
        /// <param name="mes">Сообщение</param>
        /// <param name="name">Имя темы</param>
        public CLog(string mes, string name)
        {
            Log(name, mes);
        }
        /// <summary>
        /// Запись в дефолтный лог файл.
        /// </summary>
        /// <param name="mes">Сообщение.</param>
        public static void Logging(string mes)
        {
            Log(file_def, mes);
        }
        public static void Logging(Exception mes)
        {
            Log(file_error, mes.Source + " : " + mes.Message);
        }
        public static void Logging(string mes, Type type)
        {
            Log(file_def, type.Module + " " + type.FullName + " ->" + mes);
        }
        public static void Logging(string mes, Type type, string file_name)
        {
            var dd = type.FullName;
            Log(file_name, type.Module + " " + type.FullName + " ->" + mes);
        }
        public static void Logging(string mes, string file_name)
        {
            Log(file_name, mes);
        }
        static void Log(string file_name, string message)
        {
            var list = Trace.Listeners;            
            Trace.WriteLine(LogMess(message), file_name);
            Trace.Flush();
        }
        static string LogMess(string mes)
        {
            CultureInfo rur = new CultureInfo("ru-RU");
            rur.DateTimeFormat.Calendar = new GregorianCalendar();
            var dd = DateTime.Now;
            var str = string.Format("{0} {1}", dd.ToString("d", rur) + " " + dd.ToString("t", rur), mes);
            return str;
        }
    }
}