﻿using System;

namespace KupcovKS.DataBase.AD.DllUsersInAD
{
    /// <summary>
    /// Класс связывающий DataGrid и инфу из АД.
    /// </summary>
    public class CUsersInADForDataGrid
    {
        public string ColumnOtdel { get; set; }
        public string ColumnWorker { get; set; }
        public bool IsPrint { get; set; }
        public CUsersInADForDataGrid() { }
        public CUsersInADForDataGrid(string CO, string CW)
        {
            this.ColumnOtdel = CO;
            this.ColumnWorker = CW;
            this.IsPrint = false;
        }
    }
}
