﻿using KupcovKS.Enumerable;
using KupcovKS.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.DirectoryServices;
using System.Linq;

namespace KupcovKS.DataBase.AD
{
    /// <summary>
    /// Класс <see cref="CUserInfoInAD"/> содержит в себе информацию от Active Directory о SID, CN, DEPARTMENT.
    /// И теперь синхронизируется с АД.
    /// </summary>
    public class CUserInfoInAD : INotifyPropertyChanged
    {

        public bool IsPrint { get; set; }


        public string CreateDate { get; set; }
        /// <summary>
        /// Инициализирует новый экземпляр класса Users.
        /// </summary>
        public CUserInfoInAD() { }

        /// <summary>
        /// Инициализирует новый экземпляр класса Users с помощью <seealso cref="DirectoryEntry"/>.
        /// </summary>
        /// <param name="de">DE <seealso cref="DirectoryEntry"/></param>
        public CUserInfoInAD(DirectoryEntry de)
        {
            CLog.Logging(":: " + de.Username);
            this.CN = ADFunction.AD.GetParamFromAD(de, "cn") as string;

            // var sidInBytes = (byte[])AD.GetParamFromAD(de, "objectsid");
            // var sid = new SecurityIdentifier(sidInBytes, 0);
            // This gives you what you want


            this.SID = ADFunction.AD.GetParamFromAD(de, "objectsid");
            this.DEPARTMENT = ADFunction.AD.GetParamFromAD(de, "department") as string;
            this.CreateDate = ADFunction.AD.GetParamFromAD(de, "whenCreated") as string;
            this.IsPrint = false;
        }


        /// <summary>
        /// Инициализирует новый экземпляр класса Users для копирования старых параметров в новый.
        /// </summary>
        /// <param name="U">Входящий параметр.</param>
        public CUserInfoInAD(CUserInfoInAD U)
        {
            this.SID = U.SID;
            this.CN = U.CN;
            this.DEPARTMENT = U.DEPARTMENT;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса Users для копирования старых параметров в новый.
        /// </summary>
        /// <param name="CN_">ФИО</param>
        /// <param name="SID_">SID</param>
        /// <param name="DEPARTMENT_">Отдел</param>
        public CUserInfoInAD(string CN_, string SID_, string DEPARTMENT_)
        {
            this.SID = SID_;
            this.CN = CN_;
            this.DEPARTMENT = DEPARTMENT_;
        }

        /// <summary>
        /// ФИО пользователя(сотрудника), хранящееся в AD.
        /// </summary>
        public string CN { get; set; }

        /// <summary>
        /// Номер SID.
        /// </summary>
        public string SID { get; set; }

        /// <summary>
        /// Отдел.
        /// </summary>
        public string DEPARTMENT { get; set; }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private System.Windows.Visibility _isHiden { get; set; }
        public bool IsHiden
        {
            get
            {
                if (_isHiden == System.Windows.Visibility.Hidden)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (value == true)
                {
                    this._isHiden = System.Windows.Visibility.Hidden;
                }
                else
                {
                    this._isHiden = System.Windows.Visibility.Visible;
                }
            }
        }
    }

    public class CUserInfoInADBase : List<CUserInfoInAD>
    {

        public CUserInfoInADBase() { }
        public CUserInfoInADBase(SearchResultCollection SRC) {


            foreach (SearchResult result in SRC)
            {
              //  CLog.Logging(result.ToString());
          
                if (result != null)
                {
                    this.Add(new CUserInfoInAD(result.GetDirectoryEntry()));
                }
            }
           /* for (int counter = 0; counter < SRC.Count; counter++)
            {
                if (SRC[counter] != null)
                {
                    this.Add(new CUserInfoInAD(SRC[counter].GetDirectoryEntry()));
                }
            }*/
        }
    }


    /// <summary>
    ///  Этот класс хранит информацию о сотруднике и содержит  методы по работе с ней.
    /// </summary>
    public class CWorkerInfoAD
    {

        /// <summary>
        /// Класс хранит основную информацию о сотруднике - ФИО(Фамилия Имя Отчество в одну строку через пробел).
        /// </summary> 
        ///  <example><para>Так выглядит ФИО: Иванов Иван Иванович</para></example>
        public class CPerson
        {
            /// <summary>
            ///  Конструктор без параметров. 
            /// </summary>
            public CPerson() { }

            /// <summary>
            /// Создает новый экземпляр Name_person, копию переданного.
            /// </summary>
            ///  <example><para>ФИО: Иванов Иван Иванович</para></example>
            /// <param name="N_p">Входящий</param>
            /// <seealso cref="to_doc.Name_person"/>
            public CPerson(CPerson N_p)
            {
                Family = N_p.Family;
                Name = N_p.Name;
                Othcestvo = N_p.Othcestvo;
            }

            /// <summary>
            ///  Конструктор с параметром типа <see cref="Console.WriteLine(string)"/>.
            /// </summary>
            /// <param name="FIO">Параметр типа <see cref="Console.WriteLine(string)"/> принимает ФИО(Фамилия Имя Отчество в одну строку) сотрудника и преобразует в отдельные папраметры Фамилия, Имя и Отчество.</param>
            public CPerson(string FIO)
            {
                Family = GetOrFamNameMiddle(PersonEnum.Family, FIO);
                Name = GetOrFamNameMiddle(PersonEnum.Name, FIO);
                Othcestvo = GetOrFamNameMiddle(PersonEnum.MiddleName, FIO);
            }
            /// <summary>
            /// Разделят предложение на составляющие слова.
            /// </summary>
            /// <example><para>ФИО: Иванов Иван Иванович, 0 - Иванов, 1 - Иван, 2 - Иванович</para></example>
            /// <param name="position">Принимает числа в пределах количества слов в предложении.</param>
            /// <param name="sentence">Принимает предложение</param>
            /// <returns>Возвращает в зависимости от <paramref name="chito"/> или имя, или фамилию, или отчество</returns>
            private static string GetWordFromSentence(int position, string sentence)
            {
                var str = sentence.Split(' ');
                if (str.Count() > position)
                {
                    return str[position];
                }
                else
                {
                    return null;
                }
            }
            /// <summary>
            /// Разделяет ФИО на составляющие - Фамилию, Имя,  Отчество.
            /// </summary>
            ///  <example><para>ФИО: Иванов Иван Иванович</para></example>
            /// <param name="chito">Принимает enum <see cref="PersonEnum"/>.</param>
            /// <param name="stroka">Принимает ФИО</param>
            /// <returns>Возвращает в зависимости от <paramref name="chito"/> или имя, или фамилию, или отчество</returns>
            public static string GetOrFamNameMiddle(PersonEnum chito, string stroka)
            {
                if (stroka != null)
                {
                    if (string.Compare(stroka, "") != 0)
                    {
                        if (string.Compare(stroka, " ") != 0)
                        {
                            switch (chito)
                            {
                                case PersonEnum.Family: return GetWordFromSentence(0, stroka);
                                case PersonEnum.Name: return GetWordFromSentence(1, stroka);
                                case PersonEnum.MiddleName: return GetWordFromSentence(2, stroka);
                            }

                        }
                    }
                }
                return null;
            }



            /// <summary>
            /// Формирование из ФИО сотруника в вид "Фамилия И. О." или "Фамилия И."  в зависимости от наличия отчества.
            /// </summary>
            /// <param name="FIO">ФИО человека</param>
            /// <returns>Возвращает строку в вид "Фамилия И. О." или "Фамилия И." в зависимости от наличия отчества</returns>
            public string ForInitialsN
            {
                get
                {
                    if (Name != null)
                    {
                        if (Family != null)
                        {
                            if (Othcestvo != null)
                            {
                                return Family + " " + Name[0] + ". " + Othcestvo[0] + ". ";
                            }
                            else
                            {
                                return Family + " " + Name[0] + ". ";

                            }
                        }
                    }
                    return null;
                }

            }
            /// <summary>
            /// Формирование из ФИО сотруника в вид "И. О. Фамилия" или "И. Фамилия" в зависимости от наличия отчества.
            /// </summary>
            /// <param name="FIO">ФИО человека</param>
            /// <returns>Возвращает строку в вид "И. О. Фамилия" или "И. Фамилия" в зависимости от наличия отчества</returns>    
            public string ForInitials
            {
                get
                {
                    if (Name != null)
                    {
                        if (Family != null)
                        {
                            if (Othcestvo != null)
                            {
                                return Name[0] + ". " + Othcestvo[0] + ". " + Family;
                            }
                            else
                            {
                                return Name[0] + ". " + Family;

                            }
                        }
                    }
                    return null;
                }
            }



            /// <summary>
            /// Возвращает Имя сотрудника.
            /// </summary>
            /// <returns>Возвращает Имя сотрудника.</returns>
            public string Name { set; get; }

            /// <summary>
            /// Возвращает Фамилию сотрудника.
            /// </summary>
            /// <returns>Возвращает Фамилию сотрудника.</returns>
            public string Family { set; get; }

            /// <summary>
            /// Возвращает Отчество сотрудника.
            /// </summary>
            /// <returns>Возвращает Отчество сотрудника.</returns>
            public string Othcestvo { set; get; }
        }



        /// <summary>
        /// Содержит разделенное ФИО сотрудника.
        /// <seealso cref="CPerson"/>
        /// </summary>
        public CPerson worker;
        /// <summary>
        /// Содержит разделенное ФИО начальника.
        /// <seealso cref="CPerson"/>
        /// </summary>
        public CPerson workers_boss;
        /// <summary>
        /// Конструктор без параметров.
        /// </summary>
        public CWorkerInfoAD() { }
        /// <summary>
        /// Конструктор с параметрами логин сотрудника, фио сотрудника, email сотрудника, фио начальника.
        /// </summary>
        /// <param name="login">Логин сотрудника</param>
        /// <param name="FIO">ФИО</param>
        /// <param name="mail">Email</param>
        /// <param name="nach_of_depart">ФИО начальник отдела в котором работет сотрудник.</param>
        public CWorkerInfoAD(string login, string FIO, string mail, string nach_of_depart)
        {
            this.Login = login;
            this.FIO = FIO;
            this.Email = mail;
            this.Chief = nach_of_depart;
        }
        /// <summary>
        /// Конструктор с параметрами ФИО сотрудника и ФИО начальника.
        /// Разделяет переданные ФИО на составляющие - Фамилия, имя, отчестово.
        /// </summary>
        /// <seealso cref="CPerson"/>
        /// <param name="FIO">ФИО сотрудника</param>
        /// <param name="nach_of_depart">ФИО начальника отдела</param>
        public CWorkerInfoAD(string FIO, string nach_of_depart)
        {
            this.worker = new CPerson(FIO);
            this.workers_boss = new CPerson(nach_of_depart);

        }
        /// <summary>
        /// Получает логин.
        /// </summary>
        /// <returns>Получает логин.</returns>
        public string Login { get; set; }

        /// <summary>
        /// Получает ФИО сотрудника.
        /// </summary>
        /// <returns>Получает ФИО сотрудника.</returns>
        public string FIO { get; set; }

        /// <summary>
        /// Возвращает Email.
        /// </summary>
        /// <returns>Возвращает Email.</returns>
        public string Email { get; set; }

        /// <summary>
        /// Возвращает ФИО начальника отдела.
        /// </summary>
        /// <returns>Возвращает ФИО начальника отдела.</returns>
        public string Chief { get; set; }
        //  ~NAME_id() { }
    }

    /// <summary>
    /// Работники фирмы
    /// </summary>
    public class CFirm : List<CWorkerInfoAD>
    {

        public void Add(string login, string name, string mail, string manager)
        {
            base.Add(new CWorkerInfoAD(login, name, mail, manager));
        }
        /// <summary>
        /// Добавление пользователей по параметрам.
        /// </summary>
        /// <param name="de">AD DirectoryEntry</param>
        /// <param name="login">Логин</param>
        /// <param name="name">ФИО</param>
        /// <param name="mail">Почта</param>
        /// <param name="manager">Мэнеджер</param>
        public void Add(DirectoryEntry de, string login, string name, string mail, string manager)
        {
            base.Add(new CWorkerInfoAD(ADFunction.AD.GetParamFromAD(de, login) as string, ADFunction.AD.GetParamFromAD(de, name) as string, ADFunction.AD.GetParamFromAD(de, mail) as string, ADFunction.AD.GetParamFromAD(de, manager) as string));
        }

        /// <summary>
        /// Добавление пользователей по параметрам.
        /// </summary>
        /// <param name="de">AD DirectoryEntry... "sAMAccountName", "cn", "mail", "manager"</param>
        public void Add(DirectoryEntry de)
        {
            base.Add(new CWorkerInfoAD(ADFunction.AD.GetParamFromAD(de, "sAMAccountName") as string, ADFunction.AD.GetParamFromAD(de, "cn") as string, ADFunction.AD.GetParamFromAD(de, "mail") as string, ADFunction.AD.GetParamFromAD(de, "manager") as string));
        }
    }
}
