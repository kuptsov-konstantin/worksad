﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using DataBase_local;
namespace KupcovKS.DataBase.CSV
{
    /// <summary>
    /// Пространство имен <see cref="CSV"/> класс для хранения базы данных людей прошедших идентификацию в СКД Roger.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }
    /// <summary>
    /// Класс для хранения и работы с базой данных людей прошедших идентификацию в СКД Roger.
    /// </summary>
    /// 
    public class CDataBaseForRoger
    {
        #region ПодКласс

        /// <summary>
        /// Класс содержит фамилию <see cref="CWorker._familia"/>, дату с соответствующим временем <see cref="CWorker._CurrentDay"/>, и список даты и времени <see cref="CWorker.data_lists"/>.
        /// </summary>
        public class CWorker
        {
            #region ПодКласс
            /// <summary>
            /// Дата и время. Дата как основа. Время добавляемое. 
            /// Смысл. В один день может быть несколько пометок времени, а то и одна.
            /// </summary>
            public class CCurrentDay
            { 
                #region ПодКласс
                /// <summary>
                /// Класс содержит время прихода/ухода работника.
                /// </summary>
                public class CTime
                {       
                    #region Переменные
                    /// <summary>
                    /// Время хранящеся в классе.
                    /// </summary>
                    private String _time;
                    #endregion

                    #region Конструктор
                    /// <summary>
                    /// Иницализирует класс.
                    /// </summary>
                    public CTime()
                    {
                    }

                    /// <summary>
                    /// Иницализирует класс и записывает время.
                    /// </summary>
                    /// <param name="time_">Время</param>
                    public CTime(String time_)
                    {
                        this.Time = time_;
                    }
                    #endregion
                  
                    #region Свойства
                    /// <summary>
                    /// строковое значение времени
                    /// </summary>
                    public String Time
                    {
                        set { this._time = value; }
                        get { return this._time; }
                    }
                    #endregion

                }
            
                /// <summary>
                /// Класс список со временем за текущий день
                /// </summary>
                public class CTimesInCurrentDay : List<CTime>
                {
                }
                #endregion

                #region Переменные
                /// <summary>
                /// Строковое значение даты.
                /// </summary>
                private String _data;

                /// <summary>
                /// Отметки времени за день(определенную дату).
                /// </summary>
                private CTimesInCurrentDay Times;
                #endregion

                #region Конструктор
                /// <summary>
                /// Инициализирует класс и отметки времени ( <see cref="CTime"/>)
                /// </summary>
                public CCurrentDay()
                {
                    this.Times = new CTimesInCurrentDay();
                }

                /// <summary>
                /// Инициализирует класс и добавляет новую дату и время.
                /// </summary>
                /// <param name="data_">Строковое значение даты. Формат: 01.01.2001</param>
                /// <param name="time_">Строковое значение времени. Формат: 00:00:00</param>
                public CCurrentDay(String data_, String time_)
                {
                    this.Data = data_;
                    if (this.Times == null)
                    {
                        this.Times = new CTimesInCurrentDay();
                    }
                    this.Times.Add(new CTime(time_));
                }

                /// <summary>
                /// Инициализирует класс и дебавляет время к последней дате. 
                /// Если ранее не инициализировался, то нихрена не добавит! Усек?
                /// </summary>
                /// <param name="time_">Строковое значение времени. Формат: 00:00:00</param>
                public CCurrentDay(String time_)
                {
                    if (this.Data != null)
                    {
                        this.Times.Add(new CTime(time_));
                    }
                }


                #endregion

                #region Методы

                /// <summary>
                /// Добавляет строковое значение даты и времени.
                /// </summary>
                /// <param name="data_">Строковое значение даты. Формат: 01.01.2001</param>
                /// <param name="time_">Строковое значение времени. Формат: 00:00:00</param>
                public void Add(String data_, String time_)
                {
                    this.Data = data_;
                    if (this.Times == null)
                    {
                        this.Times = new CTimesInCurrentDay();
                    }
                    this.Times.Add(new CTime(time_));
                }

                /// <summary>
                /// Добавление строкового значения времени к последней дате.
                /// Если ранее не добавлялась дата, то нихрена не добавит! Усек?
                /// </summary>
                /// <param name="time_"></param>
                public void Add(String time_)
                {
                    if (this.Data != null)
                    {
                        this.Times.Add(new CTime(time_));
                    }
                }

                #endregion

                #region Свойства
                /// <summary>
                /// Возвращает строковое значение даты.
                /// </summary>
                /// <returns></returns>
                public String Data
                {
                    set { this._data = value; }
                    get { return this._data; }
                }
               
               
                /// <summary>
                /// Возвращает отметки времени за день.
                /// </summary>
                /// <returns>Возвращает отметки времени за день.</returns>
                public CTimesInCurrentDay TimesInCurrentDay
                {
                    set { this.Times = value; }
                    get { return this.Times; }

                } 
                
                #endregion
            }


            /// <summary>
            /// Класс месяц представлен как список дат за текущий месяц 
            /// </summary>
            public class CCurrentMonth : List<CCurrentDay>
            {
            }
            #endregion

            #region Переменные
            /// <summary>
            /// Фамилия сотрудника.
            /// </summary>
            private String _familia;

            /// <summary>
            /// Временная дата с соответствующим временем.
            /// </summary>
            private CCurrentDay _CurrentDay;


            /// <summary>
            /// Список даты с соответствующим временем.(Месяц)
            /// </summary>
            private CCurrentMonth _work_days_in_current_month;

            #endregion

            #region Конструктор
            /// <summary>
            /// Инициализатор класса.
            /// </summary>
            public CWorker() { }


            //новый человек. сохраняет фамилию и инициализирует время и дату.

            /// <summary>
            /// Инициализирует вместе с фамилиией.
            /// </summary>
            /// <param name="familia_">Фамилия сотрудника.</param>
            public CWorker(String familia_)
            {
                this._familia = familia_;
                this._CurrentDay = new CCurrentDay();
                this._work_days_in_current_month = new CCurrentMonth();
            }

            #endregion

            #region Методы
            /// <summary>
            /// Добавление времени.  //добавление времени к текущему человеку в текущей дате.
            /// </summary>
            /// <param name="time_">Строковое значение времени. Формат: 00:00:00</param>
            public void Add(String time_)
            {
                this._CurrentDay.Add(time_);
            }

            /// <summary>
            /// Добавляет строковое значение даты и времени.
            /// </summary>
            /// <param name="data_">Строковое значение даты. Формат: 01.01.2001</param>
            /// <param name="time_">Строковое значение времени. Формат: 00:00:00</param>
            public void Add(String data_, String time_)
            {
                this.Update();
                this._CurrentDay = new CCurrentDay();
                this._CurrentDay.Add(data_, time_);
            }

            /// <summary>
            /// Цепочное обновление.
            /// Но тут добавляется локальный <see cref="CWorker._CurrentDay"/>  в список <see cref="CWorker.data_lists"/>
            /// </summary>
            public void Update()
            {
                this._work_days_in_current_month.Add(this._CurrentDay);
            }

            #endregion

            #region Свойства
            /// <summary>
            /// Возвращает фамилию сотрудника.
            /// </summary>
            /// <returns>Возвращает фамилию сотрудника</returns>
            public String Family
            {
                set { this._familia = value; }
                get { return this._familia; }
            }

            /// <summary>
            /// Возвращает список даты с соответствующим ей временем.
            /// </summary>
            /// <returns>Возвращает список даты с соответствующим ей временем.</returns>
            public CCurrentMonth InMonth
            {
                set { this._work_days_in_current_month = value; }
                get { return this._work_days_in_current_month; }
            }  
            #endregion
        }

        /// <summary>
        /// Список работников
        /// </summary>
        public class CEmployees : List<CWorker>
        {

        }

        #endregion

        #region Переменные
        /// <summary>
        /// Список с типом <see cref="CWorker"/>
        /// </summary>
        private CEmployees Employees;

        /// <summary>
        /// Временный. 
        /// Перед тем как добавить новую информацию в список, сначало надо добавить сюда, а потом обновить <see cref="Update"/>
        /// </summary>
        private CWorker Worker;

        #endregion

        #region Конструктор
        /// <summary>
        /// Инициализирует список и временную переменную.
        /// </summary>
        public CDataBaseForRoger()
        {
            this.Employees = new CEmployees();
            //   asd = new fam_class();
        }
        #endregion

        #region Методы 
        /// <summary>
        /// Добавление нового человека с фамилией, так же добавление к нему даты и времени.
        /// </summary>
        /// <param name="familia_">Фамилия</param>
        /// <param name="data_">Строковое значение даты. Формат: 01.01.2001</param>
        /// <param name="time_">Строковое значение времени. Формат: 00:00:00</param>
        public void Add(String familia_, String data_, String time_)
        {
            this.Worker = new CWorker(familia_);
            this.Worker.Add(data_, time_);
        }
        // добавление к нему даты и время


        /// <summary>
        /// Добавление строковых значений даты и времение.
        /// </summary>
        /// <param name="data_">Строковое значение даты. Формат: 01.01.2001</param>
        /// <param name="time_">Строковое значение времени. Формат: 00:00:00</param>
        public void Add(String data_, String time_)
        {
            this.Worker.Add(data_, time_);
        }
        //добавление времени.


        /// <summary>
        /// Добавление строкового значения времени.
        /// </summary>
        /// <param name="time_">Строковое значение времени. Формат: 00:00:00</param>
        public void Add(String time_)
        {
            this.Worker.Add(time_);
        }
        //обновление

        /// <summary>
        /// Цепочное обновление.
        /// </summary>
        public void Update()
        {
            this.Worker.Update();
            this.Employees.Add(this.Worker);
        }
        #endregion

        #region Свойства
        /// <summary>
        /// Возвращает список пользователей хранящихся в <see cref="Employees"/>.
        /// </summary>
        /// <returns>Возвращает список пользователей.</returns>
        public CEmployees List_of_users
        {
            get { return this.Employees; }
        }
        #endregion
    }
}

