﻿using KupcovKS.Logging;
using System;
using System.DirectoryServices;
using System.Globalization;
using System.IO;
using System.Security.Principal;

namespace KupcovKS.ADFunction
{
    /// <summary>
    /// Классы для работы с AD
    /// </summary>
    public class AD
    {


        /// <summary>
        /// Получение значение из записи через <see cref="System.DirectoryServices.DirectoryEntry"/> и параметр.
        /// </summary>
        /// <param name="e1">Объект иерархии доменных служб AD</param>
        /// <param name="dan">Строка параметр</param>
        /// <returns>Возвращает строковое значение</returns>
        /// <seealso cref="System.DirectoryServices.DirectoryEntry"/>
        public static string GetParamFromAD(DirectoryEntry e1, string dan)
        {
            try
            {
                if (dan.CompareTo("whenCreated") == 0)
                {
                    var WC = e1.Properties["whenCreated"].Value;
                    // CLog.Logging("WC", WC.GetType());
                    var dd = (System.DateTime)WC;
                    // File.WriteAllText("temp.oo", e1.Properties["whenCreated"].Value as String);
                    CultureInfo rur = new CultureInfo("ru-RU");
                    rur.DateTimeFormat.Calendar = new GregorianCalendar();
                   
                    var str = string.Format("{0}", dd.ToString("d", rur) + " " + dd.ToString("t", rur));

                    return str;
                }

                if (dan.CompareTo("objectsid") == 0)
                {
                    var sidInBytes = (byte[])(e1.Properties[dan]).Value;
                    var sid = new SecurityIdentifier(sidInBytes, 0);
                    // This gives you what you want
                    return sid.ToString();
                }
                else
                {
                    if ((e1.Properties[dan]).Value != null)
                        return (e1.Properties[dan]).Value.ToString();
                    else
                        return "";
                }
            }
            catch (Exception e)
            {

                CLog.Logging(e.Message, CLog.file_error);
                return "";
            }

        }
   
    }
}