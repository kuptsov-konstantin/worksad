﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KupcovKS.Print
{
    public class CPrint
    {
        public void PrintHelpPage(String addr)
        {
            WebBrowser webBrowserForPrinting = new WebBrowser();
            webBrowserForPrinting.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(PrintDocument);
            webBrowserForPrinting.Url = new Uri(addr);
        }

        private void PrintDocument(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var wb = sender as WebBrowser;
            if (wb.ReadyState.Equals(WebBrowserReadyState.Complete))
                wb.ShowPrintDialog();
            //(sender as System.Windows.Forms.WebBrowser).ShowPrintDialog();
            //(sender as System.Windows.Forms.WebBrowser).Print();
            wb.Dispose();
        }
    }
}
