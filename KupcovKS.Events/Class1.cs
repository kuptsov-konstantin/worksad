﻿using System;
using System.Windows;

namespace KupcovKS.Events
{
    /// <summary>
    /// Событие передет сообщение
    /// </summary>
    public class MessageEventArgs : EventArgs
    {
        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// На выход
        /// </summary>
        public bool isExit { get; set; }

    }
    /// <summary>
    /// Событие доступности домена
    /// </summary>
    public class AvailableDomainEventArgs : EventArgs
    {
        /// <summary>
        /// Доступность домена
        /// </summary>
        public bool isDostup { get; set; }
        /// <summary>
        /// Домен
        /// </summary>
        public string Domain { get; set; }
    }
    public class ProgressIEventArgs : EventArgs
    {
        public int Maximum { get; set; }
        public int Current { get; set; }
    }
    public class ChengedEventArgs: EventArgs
    {
        
    }
    /// <summary>
    /// Событие на изменение статус бара
    /// </summary>
    public class StatusBarEventArgs : EventArgs
    {
        /// <summary>
        /// Информационное сообщение
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Позиция прогресс бара
        /// </summary>
        public int PositionProgressBar { get; set; }
        /// <summary>
        /// Максимальное значение прогресс бара
        /// </summary>
        public int MaximumProgressBar { get; set; }

        /// <summary>
        /// Задает видимость элемента ProgressBar
        /// </summary>
        public Visibility VisibleProgressBar { get; set; }
        /// <summary>
        /// Задает видимость элемента TextBox
        /// </summary>
        public Visibility VisibleTextBox { get; set; }
        /// <summary>
        /// Задает видимость всего статус бара
        /// </summary>
        public Visibility Visible { get; set; }
    }
}
