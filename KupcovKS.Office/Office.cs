﻿using KupcovKS.Logging;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Reflection;
//using Word = Microsoft.Office.Interop.Word;

namespace KupcovKS.Office
{
    using DataBase.CSV;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using WordConverter;
    using OExcel;
    using DataBase.AD;
    using Enumerable;
    using Events;
    using System.IO;
    using DocumentFormat.OpenXml.Wordprocessing;
    using NotesFor.HtmlToOpenXml;
    using OWord;
    namespace OWord
    {
        using DocumentFormat.OpenXml.Wordprocessing;
        public class CWord
        {
            public string WordFilePuth { get; set; }

            public CWord()
            {

            }
            public CWord(string _Puth)
            {
                WordFilePuth = _Puth;
            }
            public void PrintW()
            {
                try
                {
                    Microsoft.Office.Interop.Word.Application _word = new Microsoft.Office.Interop.Word.Application();
                    var _wordDoc =  _word.Documents.Open(WordFilePuth);
                    object nullobj = Missing.Value;
                    int dialogResult = _word.Dialogs[Microsoft.Office.Interop.Word.WdWordDialog.wdDialogFilePrint].Show();
                    if (dialogResult == 1)
                    {
                        _wordDoc.PrintOut();
                    }
                }
                catch (Exception ex)
                {
                    CLog.Logging(ex.Message);

                }
            }
            public void CreateWordDoc(string htmlfileputh)
            {
                using (MemoryStream generatedDocument = new MemoryStream())
                {
                    using (WordprocessingDocument package = WordprocessingDocument.Create(generatedDocument, WordprocessingDocumentType.Document))
                    {
                        MainDocumentPart mainPart = package.MainDocumentPart;
                        if (mainPart == null)
                        {
                            mainPart = package.AddMainDocumentPart();
                            new Document(new Body()).Save(mainPart);
                        }
                        HtmlConverter converter = new HtmlConverter(mainPart);
                        var html = File.ReadAllText(htmlfileputh);
                        converter.Parse(html);
                        mainPart.Document.Save();
                    }
                    File.WriteAllBytes(WordFilePuth, generatedDocument.ToArray());
                }
                System.Diagnostics.Process.Start(WordFilePuth);
            }
        }
    }

    namespace OExcel
    {
        using DocumentFormat.OpenXml;

        using DocumentFormat.OpenXml.Spreadsheet;
        using DataBase.AD;
        using DataBase.CSV;        // using Excel = Microsoft.Office.Interop.Excel;
        using System.Linq;
        public class MyCell : Cell
        {
            CellFormats cfs = new CellFormats();
            public CellFormat Bold
            {
                set
                {
                    cfs.Append(value);
                }
            }
            public CellFormat Border
            {
                set
                {
                    cfs.Append(value);
                    this.Append(cfs);
                }
            }
            public string X { get; set; }
            public int Y { get; set; }
            public int? ColumnWidth { get; set; } = null;
            public int? RowHeight { get; set; } = null;

            public HorizontalAlignmentValues HorizontalAlignment
            {
                set
                {
                    CellFormats cf = new CellFormats();
                    Alignment alig = new Alignment() { Horizontal = value };
                    cf.Append(alig);
                    cfs.Append(cf);
                }

            }
            public VerticalAlignmentValues VerticalAlignment
            {
                set
                {
                    CellFormat cf = new CellFormat();
                    Alignment alig = new Alignment() { Vertical = value };
                    cf.Append(alig);
                    cfs.Append(cf);
                }

            }

            public NumberingFormat NumberFormat
            {
                set
                {
                    CellFormat cf = new CellFormat();
                    cf.NumberFormatId = value.NumberFormatId;
                    cfs.Append(cf);
                }
            }

            public MyCell(int x, int y)
            {
                CellReference = $"{ExcelColumnIndexToName(x)}{y}";
                X = ExcelColumnIndexToName(x);
                Y = y;
                this.Append(cfs);
            }
            public MyCell(string x, int y)
            {
                CellReference = x + y;
                X = x;
                Y = y;
                this.Append(cfs);
            }
            private string ExcelColumnIndexToName(int Index)
            {
                string range = "";
                if (Index < 0) return range;
                for (int i = 1; Index + i > 0; i = 0)
                {
                    range = ((char)(65 + Index % 26)).ToString() + range;
                    Index /= 26;
                }
                if (range.Length > 1) range = ((char)(range[0] - 1)).ToString() + range.Substring(1);
                return range;
            }

        }

        public class CExcel
        {
            /// <summary>
            /// Документ таблиц <see cref="SpreadsheetDocument"/> 
            /// </summary>
            SpreadsheetDocument spreadsheetDocument { get; set; } = null;
            /// <summary>
            /// Книга Excel <see cref="WorkbookPart"/>
            /// </summary>
            WorkbookPart workbookpart { get; set; } = null;
            public WorksheetPart WorksheetPart { get; set; } = null;
            Sheets sheets { get; set; } = null;
            SheetData sheetData { get; set; } = null;
            
    
            public CExcel(string filepath)
            {
                spreadsheetDocument = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook);
                // Add a WorkbookPart to the document.
                workbookpart = spreadsheetDocument.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();
            }
            private static void CellInsert(Worksheet worksheet, MyCell mycell)
            {
                var sheetData = worksheet.GetFirstChild<SheetData>();
                var rez = sheetData.Elements<Row>().Where(item => item.RowIndex.Value == mycell.Y).ToList();
                Row row = null;
                if (rez.Count == 0)
                {
                    row = new Row() { RowIndex = new UInt32Value((uint)mycell.Y), Height = mycell.RowHeight };
                    sheetData.Append(row);
                }
                else
                {
                    row = rez[0];
                }
                var rez_cell = row.Elements<Cell>().Where(item => item.CellReference.Value.CompareTo(mycell.CellReference.Value) == 0).ToList();
                if (rez_cell.Count > 0)
                {
                    rez_cell[0].Remove();
                }
                row.Append(mycell);
            }
            public Sheet AddSheet(string name_sheet)
            {
                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                WorksheetPart.Worksheet = new Worksheet(new SheetData());
                // Add Sheets to the Workbook.
                sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild(new Sheets());

                Sheet sheet = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(WorksheetPart), SheetId = 1, Name = name_sheet };
                sheets.Append(sheet);
                sheetData = WorksheetPart.Worksheet.GetFirstChild<SheetData>();
                return sheet;

            }

          


            private CellFormat BoldFont
            {
                get
                {
                    WorkbookStylesPart stylesPart = spreadsheetDocument.WorkbookPart.WorkbookStylesPart;

                    Font font1 = new Font(new Bold());
                    stylesPart.Stylesheet.Fonts.Append(font1);
                    stylesPart.Stylesheet.Save();

                    UInt32Value fontId = Convert.ToUInt32(stylesPart.Stylesheet.Fonts.ChildElements.Count - 1);
                    return new CellFormat() { FontId = fontId, ApplyFont = true };
                }

            }
            private CellFormat Border
            {
                get
                {
                    WorkbookStylesPart stylesPart = spreadsheetDocument.WorkbookPart.WorkbookStylesPart;
                    Border border = new Border(new BottomBorder());
                    stylesPart.Stylesheet.Borders.Append(border);
                    stylesPart.Stylesheet.Save();
                    UInt32Value fontId = Convert.ToUInt32(stylesPart.Stylesheet.Borders.ChildElements.Count - 1);
                    return new CellFormat() { BorderId = fontId, ApplyFont = true };
                }

            }
            private NumberingFormat GetNumberFormats(string format)
            {
                WorkbookStylesPart stylesPart = spreadsheetDocument.WorkbookPart.WorkbookStylesPart;
                stylesPart.Stylesheet.NumberingFormats = new NumberingFormats();
                NumberingFormat nf2decimal = new NumberingFormat();
                nf2decimal.NumberFormatId = UInt32Value.FromUInt32(3453);
                nf2decimal.FormatCode = StringValue.FromString(format);
                stylesPart.Stylesheet.NumberingFormats.Append(nf2decimal);
                return nf2decimal;
            }

            /// <summary>
            /// Создает страницу в MS Excel по переданным параметрам и определенному формату.
            ///  </summary>
            /// <param name="worksheet">Рабочая книга MS Excel.</param>
            /// <param name="DATA">Фамилия со временем.</param>
            /// <param name="USER_info">Информация о сотруднике</param>
            /// <seealso cref="Sheet"/>
            /// <seealso cref="CDataBaseForRoger.CWorker"/>
            /// <seealso cref="CWorkerInfoAD"/>      
            public void ListSheets(Sheet sheet, CDataBaseForRoger.CWorker DATA, CWorkerInfoAD USER_info)
            {
                //    String Famil = "", ima = "", otchestvo = "";
                var worksheet = WorksheetPart.Worksheet;
                string DAY_to_BEGIN = "06:00:00", FIO;
                if (USER_info != null)
                {
                    FIO = USER_info.worker.ForInitials;
                    DAY_to_BEGIN = "6:00:00";
                }
                else
                {
                    FIO = DATA.Family;
                }
                CellInsert(worksheet, new MyCell("A", 1) { ColumnWidth = 15, CellValue = new CellValue(FIO), Bold = BoldFont });
                CellInsert(worksheet, new MyCell("B", 1) { ColumnWidth = 15 });
                CellInsert(worksheet, new MyCell("C", 1) { ColumnWidth = 15, CellValue = new CellValue("Дата") });
                CellInsert(worksheet, new MyCell("D", 1) { ColumnWidth = 15, CellFormula = new CellFormula("=ТЕКСТ(A6;\"ММММ ГГГГ\")"), Bold = BoldFont, NumberFormat = GetNumberFormats("MM YY") });
                CellInsert(worksheet, new MyCell("C", 2) { CellValue = new CellValue("Норма времени") });
                CellInsert(worksheet, new MyCell("D", 2) { CellValue = new CellValue(DAY_to_BEGIN) });
                CellInsert(worksheet, new MyCell("C", 3) { CellValue = new CellValue("Обед") });
                CellInsert(worksheet, new MyCell("D", 3) { CellValue = new CellValue(string.Format("0:45:00")) });

                CellInsert(worksheet, new MyCell("A", 5) { RowHeight = 30, CellValue = new CellValue("Дата"), HorizontalAlignment = HorizontalAlignmentValues.Center, VerticalAlignment = VerticalAlignmentValues.Center/*, WrapText*/ });
                CellInsert(worksheet, new MyCell("B", 5) { CellValue = new CellValue("Начало рабочего дня"), HorizontalAlignment = HorizontalAlignmentValues.Center, VerticalAlignment = VerticalAlignmentValues.Center/*, WrapText*/ });
                CellInsert(worksheet, new MyCell("C", 5) { CellValue = new CellValue("Конец рабочего дня"), HorizontalAlignment = HorizontalAlignmentValues.Center, VerticalAlignment = VerticalAlignmentValues.Center/*, WrapText*/ });
                CellInsert(worksheet, new MyCell("D", 5) { CellValue = new CellValue("Отработано часов"), HorizontalAlignment = HorizontalAlignmentValues.Center, VerticalAlignment = VerticalAlignmentValues.Center/*, WrapText*/ });

                double per_dt = 0;

                var data_lists = DATA.InMonth;
                var count_bd = data_lists.Count;
                for (int data = 0; data < count_bd; data++)
                {
                    var perem = data_lists[data];
                    if (perem.Data != null)
                    {
                        //DATA_otch = perem.data;
                        var data_temp = Convert.ToDateTime(perem.Data);

                        CellInsert(worksheet, new MyCell("A", data + 5) { CellValue = new CellValue(perem.Data) });
                        CellInsert(worksheet, new MyCell("B", data + 5) { CellValue = new CellValue(Convert.ToDateTime(perem.TimesInCurrentDay[0].Time).TimeOfDay.ToString()) });
                        CellInsert(worksheet, new MyCell("C", data + 5) { CellValue = new CellValue(Convert.ToDateTime(perem.TimesInCurrentDay[perem.TimesInCurrentDay.Count - 1].Time).TimeOfDay.ToString()) });
                        CellInsert(worksheet, new MyCell("D", data + 5) { CellFormula = new CellFormula(string.Format(@"=ЕСЛИ(C{0}-B{0}-$D$3<=$D$3;$D$2;C{0}-B{0}-$D$3)", data + 5)), NumberFormat = GetNumberFormats("HH:MM:SS") });
                    }
                }

                CellInsert(worksheet, new MyCell("C", count_bd + 5) { CellValue = new CellValue("Всего") });
                CellInsert(worksheet, new MyCell("D", count_bd + 5) { CellFormula = new CellFormula(string.Format(@"=СУММ(D6:D{0})", count_bd + 4)), NumberFormat = GetNumberFormats("[HH]:MM") });

                CellInsert(worksheet, new MyCell("C", count_bd + 5 + 1 + 3) { CellValue = new CellValue("Итого") });
                CellInsert(worksheet, new MyCell("D", count_bd + 5 + 1 + 3) { CellFormula = new CellFormula(string.Format("=ОКРУГЛ(СУММ(D{0}:D{1})*24;0)", count_bd + 5, count_bd + 5 + 3)) });
                /// подписи

                CellInsert(worksheet, new MyCell("B", count_bd + 5 + 6) { Border = Border, Bold = BoldFont });
                CellInsert(worksheet, new MyCell("C", count_bd + 5 + 6) { Bold = BoldFont, CellFormula = new CellFormula(string.Format("=ЕСЛИ(A1<>\"\";A1;\"Сотрудник\")")) });
                CellInsert(worksheet, new MyCell("B", count_bd + 5 + 8) { Border = Border, Bold = BoldFont });

                if (USER_info != null)
                {// 21 или 23 или 4 
                    CellInsert(worksheet, new MyCell("C", count_bd + 5 + 8) { CellValue = new CellValue(USER_info.workers_boss.ForInitials), Bold = BoldFont });
                }
                else
                {
                    CellInsert(worksheet, new MyCell("C", count_bd + 5 + 8) { CellValue = new CellValue("Начальник отдела"), Bold = BoldFont });
                }
                try
                {
                    sheet.Name = USER_info.worker.ForInitialsN;
                //    worksheet.Na = USER_info.worker.for_initials_n;
                }
                catch (Exception E)
                {

                }
               // return worksheet;
            }



        }




    }
    
    public class COffice
    {
        #region 
        //  public delegate void  DProgressI(int max, int current);
        /// <summary>
        /// Ивент для визуализации прогресса
        /// </summary>
        public event EventHandler<ProgressIEventArgs> EProgressI;
        #endregion

        Dictionary<string, Action> DictionaryPrint = new Dictionary<string, Action>();

        public COffice()
        {/*
            DictionaryPrint.Add("Excel",
                () =>
                {
                    try
                    {
                        object nullobj = Missing.Value;
                        int dialogResult = _excel.Dialogs[Excel.XlBuiltInDialog.xlDialogPrintPreview].Show();
                        if (dialogResult == 1)
                        {
                            _wordDoc.PrintOut();
                        }
                    }
                    catch (Exception ex)
                    {
                        CLog.Logging(ex.Message);

                    }
                });
            DictionaryPrint.Add("Word",
                () =>
                {
                    try
                    {
                        object nullobj = Missing.Value;
                        int dialogResult = _word.Dialogs[Word.WdWordDialog.wdDialogFilePrint].Show();
                        if (dialogResult == 1)
                        {
                            _wordDoc.PrintOut();
                        }
                    }
                    catch (Exception ex)
                    {
                        CLog.Logging(ex.Message);

                    }
                });*/
        }



        public CExcel Excel { get; set; }

        public CWord Word { get; set; }


        /// <summary>
        /// Генерация документа.
        /// </summary>
        /// <param name="BD_c">База <seealso cref="CDataBaseForRoger"/></param>
        /// <param name="filepath">Путь для сохранения</param>
        public void CreateExcel(CDataBaseForRoger BD_c)
        {
            var time = DateTime.Now;
            Excel = new CExcel($"Report at {time.Day}.{time.Month}.{time.Year} {time.Hour}:{time.Minute}:{time.Second}.xlsx");
            var list_of_users = BD_c.List_of_users;
            for (int i = 0; i < list_of_users.Count; i++)
            { 
                EProgressI(this, new ProgressIEventArgs() { Maximum = list_of_users.Count, Current = i });
                var lastname = CWorkerInfoAD.CPerson.GetOrFamNameMiddle(PersonEnum.Family, list_of_users[i].Family);
                var firstname = CWorkerInfoAD.CPerson.GetOrFamNameMiddle(PersonEnum.Name, list_of_users[i].Family);
                var rez = CUserCard.GetWorkerInfo(firstname, lastname);
                Excel.ListSheets(Excel.AddSheet(rez.worker.ForInitialsN), list_of_users[i], rez);
            }
        //    this.ExcelApp.Visible = true;//// делает видимым окно экселя...   */
        }
        public void CreateWord(string new_puth)
        {
            string filename = $"{Path.GetFileName(new_puth)}.docx";
            Word = new CWord() {WordFilePuth = filename };
            Word.CreateWordDoc(new_puth);
        }
    

        // Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Excel.Application\\CurVer",null,null);
        public static string ExcelAvalaible
        {
            get
            {
                var result = Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Excel.Application\\CurVer", null, null);
                if (result != null)
                {
                    return (result as string);
                }
                else
                {
                    return null;
                }


            }
        }
        public static string WorldAvalaible
        {
            get
            {
                var result = Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Word.Application\\CurVer", null, null);
                if (result != null)
                {
                    return (result as string);
                }
                else
                {
                    return null;
                }


            }
        }
    }

}
