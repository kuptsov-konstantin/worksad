﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using KupcovKS.DataBase.AD;
using KupcovKS.WinApi;
using KupcovKS.Logging;
using KupcovKS.Events;
using KupcovKS.WordConverter;
using KupcovKS.Office;

namespace KupcovKS.UserCardCreator
{


    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Делегат закрытия окна.
        /// <summary>
        /// Делегат закрытия окна
        /// </summary>
       // public delegate void DCloseWindow();
        //   public DCloseWindow CloseWindow;
        /// <summary>
        /// Метод закрытия окна.
        /// </summary>
        public void MCloseWindow()
        {
            StatusBar("Выход", 0, 1);
            this.Close();
        }
        #endregion

        #region  Делегат и ивент на статус
      //  public delegate void DStatusBar(string status, int pos, int max);
        public event EventHandler<StatusBarEventArgs> EStatusBar;
      /*  public void MStatusBar(string status, int pos, int max)
        {
            this.StatusText.Text = status;
            this.StatusProgress.Maximum = max;
            this.StatusProgress.Value = pos;
        }
        */
        /// <summary>
        /// Установка статус бара и прогресс бара
        /// </summary>
        /// <param name="status">Статус сообщение</param>
        /// <param name="pos"></param>
        /// <param name="max"></param>
        public void StatusBar(string status, int pos, int max)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(/*new DStatusBar(MStatusBar), status, pos, max*/ () => {
                    this.StatusText.Text = status;
                    this.StatusProgress.Maximum = max;
                    this.StatusProgress.Value = pos;
                });
                
            }
        }
        #endregion

        #region добавление в datagrid
        // public delegate void DAddDataGrid(CUserInfoInAD user);
        /// <summary>
        /// Метод позволяет добавить строку <see cref="CUserInfoInAD"/> в <see cref="DataGrid"/> окна
        /// </summary>
        /// <param name="user"></param>
        public void MAddDataGrid(CUserInfoInAD user)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(
                    () =>
                    {
                        this.DataGridEmpl.Items.Add(user);
                    });
            }
        }
        #endregion
        /// <summary>
        /// Хранит получаемый список пользователей <see cref="DataBase.AD.CUserInfoInAD"/>          
        /// </summary>
        /// <seealso cref="DataBase.AD.CUserInfoInAD"/>
        List<CUserInfoInAD> List_USERS_in_gruop;
        /// <summary>
        /// Потоки
        /// </summary>
        /// <seealso cref="System.Threading.Thread"/>
        Thread TH, load_users;

        CUserCard userCard;

        ObservableCollection<CUserInfoInAD> custdata;

        CUserInfoInADBase Users;
        CWinApi cwa;
        /// <summary>
        /// Инициализируется основное окно и поключается к AD от имени учетной записи.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            var directory = Environment.CurrentDirectory;
            try
            {
                string appPath = System.IO.Directory.GetCurrentDirectory();
                cwa = new CWinApi(appPath + '\\' + "settings.ini");
            }
            catch (Exception es)
            {
                CLog.Logging(System.IO.Directory.GetCurrentDirectory() + '\\' + "settings.ini");
                CLog.Logging(es);
            }
    
            custdata = new ObservableCollection<CUserInfoInAD>();

            //Bind the DataGrid to the customer data
            DataGridEmpl.ItemsSource = custdata;
            path_shablon.Text = cwa.INIReadValue("Global_settings", "template_html");
            this.userCard = new CUserCard();
            this.userCard.EAvailableDomain += asdf_EDomainIsDostupen;
            this.userCard.EMessage += asdf_EMessage;
            // this.TH2 = new Thread(init_CUserCard);
            //  this.TH2.Start(this.userCard);    
            this.userCard.Initialize();
        }
        void init_CUserCard(object obj)
        {
            // var cuc = obj as to_doc.CUserCard;
            (obj as CUserCard).Initialize();

        }
        void asdf_EMessage(object sender, MessageEventArgs e)
        {
            if (e.isExit == true)
            {
                StatusBar(e.Message, 0, 1);
                MessageBox.Show(e.Message);
                if (!Dispatcher.CheckAccess())
                {
                    Dispatcher.Invoke(()=> { MCloseWindow(); });
                    return;
                }
                else
                {
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show(e.Message);
            }
        }


        List<String> Dep = null;


        /// <summary>
        /// Активируется при присоединении к домену AD или ошибке.
        /// </summary>
        /// <param name="e">Событие типа <see cref="AvailableDomainEventArgs"/></param>
        void asdf_EDomainIsDostupen(object sender, AvailableDomainEventArgs e)
        {

            if (e.isDostup == true)
            {
                StatusBar(e.Domain, 0, 0);
                Users = this.userCard.GetAllUsers();
                Dep = this.userCard.GetAllDepartment();
                this.LW.Items.Clear();
                this.LW.Items.Add("Все отделы");
                CLog.Logging("Dep : " + Convert.ToString(Dep.Count));
                for (int i = 0; i < Dep.Count; i++)
                {
                    CLog.Logging(i +": " + Dep[i]);
                    this.LW.Items.Add(Dep[i]);
                }
                this.custdata.Clear();
                CLog.Logging(Convert.ToString("Users : " + Users.Count));
                for (int i = 0; i < Users.Count; i++)
                {
                    CLog.Logging(i + ": " + Users[i].CN);
                    //this.custdata.Add(new CUserInfoInAD(CU[i]));
                    this.custdata.Add(Users[i]);
                }
                CLog.Logging("depends...");
                CLog.Logging(Convert.ToString(this.custdata.Count));
            }
        }


        /*формирование карточки*/
        /// <summary>
        /// Действие - формирование карточки пользователя из AD в MS Word. Подробнее <see cref="CUserCard.HTML_to_doc"/> 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (path_shablon.Text.CompareTo("") == 0) {
                MessageBox.Show("Укажите путь к шаблону!!!!!");
                return;
            }
            if (quickReportCalendarCheckedRadioButton.Name.CompareTo("RadioPrintAll") == 0)
            {
                COffice office = new COffice();
                for (int i = 0; i < custdata.Count; i++)
                {
                    if (custdata[i].IsPrint == true)
                    {
                        var puth = userCard.GetHTMLTemplate(custdata[i].SID, true, path_shablon.Text);
                        var new_puth = userCard.UpdateTextAreasInHTML(puth);
                      
                        office.CreateWord(new_puth);
                        custdata[i].IsPrint = false;
                    }
                }
                office.Word.PrintW();
            }
            if (quickReportCalendarCheckedRadioButton.Name.CompareTo("RadioPredProsm") == 0)
            {
                for (int i = 0; i < this.custdata.Count; i++)
                {
                    if (this.custdata[i].IsPrint == true)
                    {
                        // MessageBox.Show(this.custdata[i].SID);
                        var puth = this.userCard.GetHTMLTemplate(this.custdata[i].SID, true, path_shablon.Text);
                        NewWindowHandler(puth);
                        //  predrosmotr ped = new predrosmotr(puth);
                        // ped.Show(); 
                        this.custdata[i].IsPrint = false;
                    }

                }
            }
            if (quickReportCalendarCheckedRadioButton.Name.CompareTo("RadioMSWord") == 0)
            {
                for (int i = 0; i < this.custdata.Count; i++)
                {
                    if (this.custdata[i].IsPrint == true)
                    {
                        // MessageBox.Show(this.custdata[i].SID);
                        var puth = this.userCard.GetHTMLTemplate(this.custdata[i].SID, true, path_shablon.Text);
                        this.userCard.UpdateTextAreasInHTML(puth);


                        //  predrosmotr ped = new predrosmotr(puth);
                        // ped.Show(); 
                        this.custdata[i].IsPrint = false;
                    }

                }
            }


            // var USERs = this.List_USERS_in_gruop[namess.SelectedIndex];
            // var user2 = this.asdf.GetUSERbySID(USERs.SID);
            //  this.userCard.HTML_to_doc(USERs.SID);

        }
        private void NewWindowHandler(CUserCard.CReturn str)
        {
            Thread newWindowThread = new Thread(
                (obj) =>
                {
                    var dd = obj as CUserCard.CReturn;
                    predrosmotr tempWindow = new predrosmotr(dd);
                    tempWindow.Show();
                    System.Windows.Threading.Dispatcher.Run();
                });
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start(str);
        }    
        private void namess_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        /*групы */
        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //  this.INIT_HOD();
            this.load_users = new Thread(load_users_thread);
            this.load_users.SetApartmentState(ApartmentState.STA);
            this.load_users.Start();
        }
        void load_users_thread()
        {
            /*int pos = 0;
             Dispatcher.BeginInvoke(new ThreadStart(delegate
             {
                 namess.Items.Clear();
                 pos = comboBox1.SelectedIndex;
             }));
             var GGL = this.userCard.GetAllDep();
             var department = GGL[pos];
             this.List_USERS_in_gruop = new List<CUserInfoInAD>();
             for (int u = 0; u < this.userCard.UsersOnList.Count; u++)
             {
                 this.CURRENT = u;
                 this.MAXIMUM = this.userCard.UsersOnList.Count;
                 var USER = this.userCard.UsersOnList[u];
                 if (String.Compare(USER.DEPARTMENT, department) == 0)
                 {
                     this.List_USERS_in_gruop.Add(new CUserInfoInAD(USER));
                     var user2 = this.userCard.GetUSERbySID(USER.SID);
                     Dispatcher.BeginInvoke(
                         new ThreadStart(
                             delegate
                             {
                                 try
                                 {
                                     if (user2.Boss_of_dep != null)
                                         namess.Items.Add(user2.FIO + " (" + user2.Login + ") " + user2.Boss_of_dep);
                                     else
                                         namess.Items.Add(user2.FIO + " (" + user2.Login + ") ");
                                     if (namess.Items.Count > 0)
                                     {
                                         namess.SelectedIndex = 0;
                                     }
                                 }
                                 catch (Exception E)
                                 {
                                 }
                             }));
                 }
             }*/

        }

        /*Выход*/
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Close();
        }



        void OnChecked(object sender, RoutedEventArgs e)
        {
            //System.Windows.MessageBox.Show("Ку");
        }

        public Thread TH2 { get; set; }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

       // public delegate void DDataGridEmplAdd(CUserInfoInAD cuiiad);
        public void MDataGridEmplAdd(CUserInfoInAD cuiiad)
        {

            this.DataGridEmpl.Items.Add(cuiiad);
        }
        public void DataGridEmplAdd(CUserInfoInAD cuiiad)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() =>
                {
                    MDataGridEmplAdd(cuiiad);
                });
                return;
            }
            else
            {
                MDataGridEmplAdd(cuiiad);
            }
        }

     //   public delegate void DDataGridEmplClear();
        public void MDataGridEmplClear() {
            this.DataGridEmpl.Items.Clear();
        }
        public void DataGridEmplClear()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => {
                    MDataGridEmplClear();
                });           
            }
            else
            {
                MDataGridEmplClear();
            }
        }
        private void LW_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                // System.Windows.MessageBox.Show("LW_Selected");
                DataGridEmplClear();
                if (LW.SelectedIndex == 0)
                {
                    for (int i = 0; i < this.Users.Count; i++)
                    {
                        DataGridEmplAdd(Users[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < this.Users.Count; i++)
                    {
                        if (this.Users[i].DEPARTMENT.CompareTo(this.LW.SelectedItem) == 0)
                        {
                            DataGridEmplAdd(Users[i]);
                        }
                    }
                }
            }
            catch (Exception e1)
            {                
                CLog.Logging(e1);
            }
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton ck = sender as RadioButton;
            if (ck.IsChecked.Value)
                quickReportCalendarCheckedRadioButton = ck;

        }

        public RadioButton quickReportCalendarCheckedRadioButton { get; set; }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.custdata.Clear();
            foreach (var user in this.Users)
            {
                if (user.CN.LastIndexOf(((TextBox)sender).Text) > -1)
                {
                    this.custdata.Add(user);
                }
            }
            /*for (int i = 0; i < this.Users.Count; i++)
            {                
                if (this.Users[i].CN.LastIndexOf(TB.Text) > -1)
                {
                    this.custdata.Add(Users[i]);
                }
            }*/
        }

        private void open_shablon_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".html";
            dlg.Filter = "HTML шаблон (.html)|*.html";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                path_shablon.Text = filename;
                cwa.INIWriteValue("Global_settings", "template_html", filename);
            }
        
        }
    }
}
