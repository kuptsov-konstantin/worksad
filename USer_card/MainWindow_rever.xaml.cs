﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using KupcovKS.Logging;
using KupcovKS.WinApi;
using KupcovKS.DataBase.AD;
using System.Threading;
using System.Collections.ObjectModel;
using KupcovKS.WordConverter;
using KupcovKS.Events;
using System.IO;
using System.Windows.Media;
using KupcovKS.Office;

namespace KupcovKS.UserCardCreator
{

    /// <summary>
    /// Логика взаимодействия для MainWindow_rever.xaml
    /// </summary>   
    public partial class MainWindow_rever : Window
    {
        Dictionary<string, Action> DictionaryRB = new Dictionary<string, Action>();
        Dictionary<string, Action> DictionaryPrint = new Dictionary<string, Action>();

        #region Делегат закрытия окна.
        //   public DCloseWindow CloseWindow;
        /// <summary>
        /// Метод закрытия окна.
        /// </summary>
        public void MCloseWindow()
        {
            StatusBar(Status: "Выход", position: 0, maximum: 1);
            this.Dispatcher.Invoke(() => { Close(); });
        }
        #endregion

        #region  Ивент на статус
        /// <summary>
        /// Событие на статус бар
        /// </summary>
        public event EventHandler<StatusBarEventArgs> EStatusBar;
        public void StatusBar(Visibility visible_statusbar)
        {
          //  this.StatusText.Dispatcher.Invoke(() => this.StatusText.Text = Status);
            this.StatusBarControl.Dispatcher.Invoke(
                () =>
                {
                    this.StatusBarControl.Visibility = visible_statusbar;
                });

        }
        public void StatusBar(string Status, Visibility visible_StatusProgress)
        {
            this.StatusText.Dispatcher.Invoke(() => this.StatusText.Text = Status);
            this.StatusProgress.Dispatcher.Invoke(
                () =>
                {
                    this.StatusProgress.Visibility = visible_StatusProgress;
                });

        }
        public void StatusBar(string Status, int position, int maximum)
        {
            this.StatusText.Dispatcher.Invoke(() => this.StatusText.Text = Status);
            this.StatusProgress.Dispatcher.Invoke(
                () =>
                {
                    this.StatusProgress.Maximum = maximum;
                    this.StatusProgress.Value = position;
                });
        }
        #endregion

        #region добавление в datagrid
        public void MAddDataGrid(CUserInfoInAD User)
        {
            this.DataGridEmpl.Items.Add(User);
        }
        #endregion
        /// <summary>
        /// Хранит получаемый список пользователей <see cref="CUserInfoInAD"/>          
        /// </summary>
        /// <seealso cref="CUserInfoInAD"/>
        List<CUserInfoInAD> List_USERS_in_gruop;
        /// <summary>
        /// Потоки
        /// </summary>
        /// <seealso cref="System.Threading.Thread"/>
        Thread TH, load_users;

        CUserCard userCard;

        /// <summary>
        /// Переменные для ProgressBar.
        /// </summary>
        int CURRENT = 0, MAXIMUM = 0;

        ObservableCollection<CUserInfoInAD> custdata;

        CUserInfoInADBase Users;
        CWinApi cwa;

        private string name_admin = "";
        string CurrentUserName
        {     
            set{
                name_admin = $"Даровки {value}!" ;
            }
            get { return name_admin; }
        }

        public MainWindow_rever()
        {
            InitializeComponent();

            var directory = Environment.CurrentDirectory;
            try
            {
                string appPath = System.IO.Directory.GetCurrentDirectory();
                cwa = new CWinApi(appPath + '\\' + "settings.ini");
            }
            catch (Exception es)
            {
                CLog.Logging(System.IO.Directory.GetCurrentDirectory() + '\\' + "settings.ini");
                CLog.Logging(es);
            }

            custdata = new ObservableCollection<CUserInfoInAD>();

            //Bind the DataGrid to the customer data
            DataGridEmpl.ItemsSource = custdata;
            path_shablon.Text = Path.GetFileName( cwa.INIReadValue("Global_settings", "template_html"));
            this.userCard = new CUserCard();
            this.userCard.EAvailableDomain += asdf_EDomainIsDostupen;
            this.userCard.EMessage += asdf_EMessage;
            // this.TH2 = new Thread(init_CUserCard);
            //  this.TH2.Start(this.userCard);    
            this.userCard.Initialize();
        }
        /// <summary>
        /// Активируется при присоединении к домену AD или ошибке.
        /// </summary>
        private void asdf_EDomainIsDostupen(object sender, AvailableDomainEventArgs e)
        {

            if (e.isDostup == true)
            {
                CurrentUserName = this.userCard.LoggedUserName;
                this.LoggedUserName.Content = CurrentUserName;
                StatusBar(e.Domain, Visibility.Hidden);
                Users = this.userCard.GetAllUsers();
                Dep = this.userCard.GetAllDepartment();
                this.LW.Items.Clear();
                this.LW.Items.Add("Все отделы");
                CLog.Logging("Dep : " + Convert.ToString(Dep.Count));
                for (int i = 0; i < Dep.Count; i++)
                {
                    CLog.Logging(i + ": " + Dep[i]);
                    this.LW.Items.Add(Dep[i]);
                }
                this.custdata.Clear();
                CLog.Logging(Convert.ToString("Users : " + Users.Count));
                for (int i = 0; i < Users.Count; i++)
                {
                    CLog.Logging(i + ": " + Users[i].CN);
                    //this.custdata.Add(new CUserInfoInAD(CU[i]));
                    this.custdata.Add(Users[i]);
                }
                CLog.Logging("depends...");
                CLog.Logging(Convert.ToString(this.custdata.Count));
            }
        }
 
   

        void init_CUserCard(object obj)
        {
            // var cuc = obj as to_doc.CUserCard;
            (obj as CUserCard).Initialize();

        }
        void asdf_EMessage(object sender, MessageEventArgs e)
        {
            if (e.isExit == true)
            {
                StatusBar(e.Message, 0, 1);
                MessageBox.Show(e.Message);
                MCloseWindow();
            }
            else
            {
                MessageBox.Show(e.Message);
            }
        }


        List<string> Dep = null;

    
        string PrintRadioButtonName = "";
        /*формирование карточки*/
        /// <summary>
        /// Действие - формирование карточки пользователя из AD в MS Word./> 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, RoutedEventArgs e)
        {
                    
            DictionaryPrint[PrintRadioButtonName].Invoke();
        }
        private void NewWindowHandler(CUserCard.CReturn str)
        {
            //  Thread newWindowThread = new Thread(ThreadStartingPoint);
            Thread newWindowThread = new Thread(
                (obj) =>
                {
                    var dd = obj as CUserCard.CReturn;
                    predrosmotr tempWindow = new predrosmotr(dd);
                    tempWindow.Show();
                    System.Windows.Threading.Dispatcher.Run();
                });
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start(str);
        }

        private void namess_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
 
      
        /*Выход*/
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Close();
        }



        void OnChecked(object sender, RoutedEventArgs e)
        {
            //System.Windows.MessageBox.Show("Ку");
        }

        public Thread TH2 { get; set; }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Добаление записи в DataGridEmpl
        /// </summary>
        /// <param name="cuiiad"></param>
        public void MDataGridEmplAdd(CUserInfoInAD cuiiad)
        {

            this.DataGridEmpl.Dispatcher.Invoke(
                () =>
                {
                    this.DataGridEmpl.Items.Add(cuiiad);
                });
        }


        /// <summary>
        /// Очистка DataGridEmpl
        /// </summary>
        public void MDataGridEmplClear()
        {
            this.DataGridEmpl.Dispatcher.Invoke(
                 () =>
                 {
                     this.DataGridEmpl.Items.Clear();
                 });
        }
       
        private void LW_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                this.custdata.Clear();
                if (LW.SelectedIndex == 0)
                {
                    for (int i = 0; i < this.Users.Count; i++)
                    {
                        this.custdata.Add(this.Users[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < this.Users.Count; i++)
                    {
                        if (this.Users[i].DEPARTMENT.CompareTo(this.LW.SelectedItem) == 0)
                        {
                            this.custdata.Add(this.Users[i]);
                        }
                        else
                        {
                          //  this.custdata[i].IsHiden = true;
                        }
                    }
                }
                
            }
            catch (Exception e1)
            {
                CLog.Logging(e1);
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton ck = sender as RadioButton;
            if (ck.IsChecked.Value)
            {
                PrintRadioButtonName = ck.Name;
                if (DictionaryRB.Count > 0)
                {
                    DictionaryRB[ck.Name].Invoke();                
                }
            }
        }
       
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DictionaryRB.Add("RadioPrintAll",
               () =>
               {
                   PrintButton.Dispatcher.Invoke(() =>
                   {
                       PrintButton.Content = "Передать все записи в MS Word";
                   });
               });
            DictionaryRB.Add("RadioPredProsm",
                () =>
                {
                    PrintButton.Dispatcher.Invoke(() =>
                    {
                        PrintButton.Content = "Предпросмотр отмеченных";
                    });
                });

            DictionaryRB.Add("RadioMSWord",
                () =>
                {
                    PrintButton.Dispatcher.Invoke(() =>
                    {
                        PrintButton.Content = "Передать отмеченные записи в MS Word";
                    });
                });

            DictionaryPrint.Add("RadioPrintAll",
                () =>
                {
                    var tamlate_file = cwa.INIReadValue("Global_settings", "template_html");
                    if (tamlate_file.CompareTo("") == 0)
                    {
                        MessageBox.Show("Укажите путь к шаблону!!!!!");
                        return;
                    }
                    for (int i = 0; i < custdata.Count; i++)
                    {

                        if (custdata[i].IsPrint == true)
                        {
                            var puth = this.userCard.GetHTMLTemplate(this.custdata[i].SID, true, tamlate_file);
                            var res = this.userCard.UpdateTextAreasInHTML(puth);
                            if (res != string.Empty)
                            {
                                COffice coffice = new COffice();
                                coffice.CreateWord(res);
                                custdata[i].IsPrint = false;
                                coffice.Word.PrintW();
                            }
                            else
                            {
                                return;
                            }
                            
                        }
                    }
         
                });
            DictionaryPrint.Add("RadioPredProsm",
                () =>
                {
                    var tamlate_file = cwa.INIReadValue("Global_settings", "template_html");
                    if (tamlate_file.CompareTo("") == 0)
                    {
                        MessageBox.Show("Укажите путь к шаблону!!!!!");
                        return;
                    }
                    StatusBar("Выполнение", Visibility.Visible);
                   
                    for (int i = 0; i < this.custdata.Count; i++)
                    {
                        if (this.custdata[i].IsPrint == true)
                        {
                            StatusBar($"Выполнение. {custdata[i].CN}", 0, this.custdata.Count);
                            var puth = this.userCard.GetHTMLTemplate(this.custdata[i].SID, true, tamlate_file);
                            NewWindowHandler(puth);
                            this.custdata[i].IsPrint = false;
                        }

                    }
                    StatusBar("Выполнено", Visibility.Hidden);
                });

            DictionaryPrint.Add("RadioMSWord",
                () =>
                {
                    var tamlate_file = cwa.INIReadValue("Global_settings", "template_html");
                    if (tamlate_file.CompareTo("") == 0)
                    {
                        MessageBox.Show("Укажите путь к шаблону!!!!!");
                        return;
                    }
                    for (int i = 0; i < this.custdata.Count; i++)
                    {
                        if (this.custdata[i].IsPrint == true)
                        {
                            var puth = this.userCard.GetHTMLTemplate(this.custdata[i].SID, true, tamlate_file);
                            this.userCard.UpdateTextAreasInHTML(puth);
                            this.custdata[i].IsPrint = false;
                        }

                    }
                });

        }

        private void WordLabel_Loaded(object sender, RoutedEventArgs e)
        {
            if (COffice.WorldAvalaible!= null )
            {
                SolidColorBrush brush = new SolidColorBrush(Colors.Green);
                WordLabel.Content = COffice.WorldAvalaible;
                WordLabel.Foreground = brush;
            }
            else
            {
                SolidColorBrush brush = new SolidColorBrush(Colors.Red);
                WordLabel.Content = "MS Word не обнаружен!";               
                WordLabel.Foreground = brush;
                RadioPrintAll.IsEnabled = false;
                RadioPredProsm.IsChecked = true;
                RadioMSWord.IsEnabled = false;
            }
           
        }

        private void ExcelLabel_Loaded(object sender, RoutedEventArgs e)
        {
            if (COffice.ExcelAvalaible != null)
            {
                SolidColorBrush brush = new SolidColorBrush(Colors.Green);
                ExcelLabel.Content = COffice.ExcelAvalaible;
                ExcelLabel.Foreground = brush;
            }
            else
            {
                SolidColorBrush brush = new SolidColorBrush(Colors.Red);
                ExcelLabel.Content = "MS Excel не обнаружен!";             
                ExcelLabel.Foreground = brush;
            }
        }

        private void ExitMI_Click(object sender, RoutedEventArgs e)
        {
            MCloseWindow();
        }

        private void AboutMI_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow cw = new AboutWindow();
            cw.ShowInTaskbar = false;
            cw.Owner = Application.Current.MainWindow;
            cw.Show();
        }

        private void HelpMI_Click(object sender, RoutedEventArgs e)
        {

        }

        private void UpdateMI_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.custdata.Clear();
            foreach (var user in this.Users)
            {
                if (user.CN.LastIndexOf(((TextBox)sender).Text) > -1)
                {
                   this.custdata.Add(user);
                 //   user.IsHiden = false;
                }
                else
                {
                   // user.IsHiden = true;
                }
            }
  
        }

        private void open_shablon_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".html";
            dlg.Filter = "HTML шаблон (.html)|*.html";
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                path_shablon.Text  = Path.GetFileName(filename);
                cwa.INIWriteValue("Global_settings", "template_html", filename);
            }

        }
  
    }
}
