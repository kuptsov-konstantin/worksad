﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Excel = Microsoft.Office.Interop.Excel;
//using Office = Microsoft.Office.Core;
using System.IO;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using KupcovKS.DataBase.CSV;
using KupcovKS.DataBase.AD;
using Microsoft.Vbe.Interop;
using System.Windows;
using KupcovKS.WordConverter;
using KupcovKS.Events;
using KupcovKS.Office;
using KupcovKS.Logging;

namespace KupcovKS.CSVInterpritation
{

    /// <summary>
    /// Класс для формирования отчета  в виде Excel документа из отчета в виде csv файла сгенерированного СКД Roger .
    /// Так же для своих нужд пожно использовать методы.
    /// </summary>
    public class CWorkWithExcel
    {
 
        /*перевод из csv в базу данных*/
        /// <summary>
        /// Открытие отчета СКД Roger с форматом "Фамилия|День|Время" и запись в <see cref="CDataBaseForRoger"/>
        /// </summary>
        /// <param name="put">Путь где лежит файл СКД Roger </param>
        public static CDataBaseForRoger CsvToDB(string put)
        {
            CDataBaseForRoger BD_c = new CDataBaseForRoger();
            try
            {              
                var VAL = File.ReadAllBytes(put);
                string values = File.ReadAllText(put, Encoding.Default);
                var ad = values.Split('\r', '\n');
                Array.Sort(ad);
                int fd = 0;
                for (int i = 0; i < ad.Length; i++)
                {
                    if (string.Compare(ad[i], "") != 0)
                    {
                        fd = i;
                        break;
                    }
                }
                var new_str = new string[ad.Length - fd];
                for (int i = 0; i < new_str.Length; i++)
                {
                    new_str[i] = ad[fd + i];
                }
                var saf = new string[new_str.Length, 3];
                for (int i = 0; i < new_str.Length; i++)
                {
                    if (i == 2035)
                    {
                    }
                    var hhahhi = new_str[i].Split(';');
                    for (int j = 0; j < 3; j++)
                    {
                        saf[i, j] = hhahhi[j];
                    }
                    //фамилия  //день    //время
                    if (i - 1 >= 0)
                    {
                        //сравнение с фамилией.
                        if (string.Compare(saf[i - 1, 0], saf[i, 0]) == 0)
                        {
                            //сравнение с датой.
                            if (string.Compare(saf[i - 1, 1], saf[i, 1]) == 0)
                            {
                                BD_c.Add(saf[i, 2]);
                            }
                            else
                            {
                                BD_c.Add(saf[i, 1], saf[i, 2]);
                            }
                        }
                        else
                        {
                            BD_c.Update();
                            BD_c.Add(saf[i, 0], saf[i, 1], saf[i, 2]);
                        }
                    }
                    else
                    {
                        BD_c.Add(saf[i, 0], saf[i, 1], saf[i, 2]);
                    }
                }
                BD_c.Update();
            }
            catch (Exception e1)
            {
                BD_c = null;
                CLog.Logging(e1);
            }
           return BD_c;
        }
    }   
}
