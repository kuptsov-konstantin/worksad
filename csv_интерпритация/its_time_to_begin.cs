﻿namespace KupcovKS.CSVInterpritation
{
    /// <summary>
    /// Хранение времени. its_time_to_begin.
    /// </summary>
    public class CTimeС
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public CTimeС()
        {

        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="hour">Часы</param>
        /// <param name="minute">Минуты</param>
        /// <param name="second">Секунды</param>
        public CTimeС(int hour, int minute, int second)
        {
            this.Hour = hour;
            this.Minute = minute;
            this.Second = second;
        }

        /// <summary>
        /// Час
        /// </summary>
        public int Hour { get; set; } = 0;

        /// <summary>
        /// Минуты
        /// </summary>
        public int Minute { get; set; } = 0;

        /// <summary>
        /// Секунды
        /// </summary>
        public int Second { get; set; } = 0;

    }
}