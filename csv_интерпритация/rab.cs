﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KupcovKS.CSVInterpritation
{
    using KupcovKS.CSVInterpritation;
    using List_obj = System.Collections.ObjectModel.ObservableCollection<Object_i>;
    using rab_list = System.Collections.ObjectModel.ObservableCollection<rab>;

    /// <summary>
    /// Пространство имен <see cref="KupcovKS.CSVInterpritation"/> содержит следующие классы....
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }


 
    /// <summary>
    /// Сохранение/открытие списка в XML.
    /// </summary>
    static public class savii
    {
        /// <summary>
        /// Сохранение объекта <see cref="Object"/>, скорее списка.
        /// </summary>
        /// <param name="file">Путь файла (*.xml) куда следует сохранить.</param>
        /// <param name="myObject">Сохраняемый объект.</param>
        public static void save_calss(String file, Object myObject)
        {
            try
            {
                var mySerializer = new XmlSerializer(myObject.GetType());
                var myWriter = new StreamWriter(file);
                mySerializer.Serialize(myWriter, myObject);
                myWriter.Close();
            }
            catch (Exception er)
            {
                //System.Windows.MessageBox.Show(er.ToString(), "Ошибка");
            }
        }
        //"rabotniki.xml"

        /// <summary>
        /// Открытие xml документа.
        /// </summary>
        /// <param name="file">Путь файла (*.xml).</param>
        /// <param name="types">Тип получаемого объекта.</param>
        /// <returns>Возвращает объект определенного типа.</returns>
        public static Object open_class(String file, Type types)
        {
            try
            {
                var myWriter = new StreamReader(file);
                var mySerializer = new XmlSerializer(types);
                var myObject = (mySerializer.Deserialize(myWriter));
                myWriter.Close();
                return myObject;
            }
            catch (FileNotFoundException er)
            {
                //System.Windows.MessageBox.Show(er.ToString(), "Ошибка");
            }
            catch (Exception er)
            {
                //System.Windows.MessageBox.Show(er.ToString(), "Ошибка");
            }  
            return null;
        }
    }

    /// <summary>
    /// Класс содержит класс списков должностей и отделов которые есть в фирме.
    /// Скорее всего этот класс не нужен, потому что ввелось AD с некоторой версии программы.
    /// </summary>
    public class organizacia
    {
        /// <summary>
        /// Объект класса <see cref="CListsDep"/>
        /// </summary>
        private CListsDep _list_dolzn_otdels;

        /// <summary>
        /// Подкласс содержит списки отделов.
        /// </summary>
        public class CListsDep
        {
            /// <summary>
            /// Список должностей.
            /// </summary>
            private List<String> _dolzn;

            /// <summary>
            /// Список отделов.
            /// </summary>
            private List<String> _otdels;

            /// <summary>
            /// Инициализирует списки.
            /// </summary>
            public CListsDep()
            {

                this.Dolzn = new List<String>();
                this.Otdels = new List<String>();
            }


            /// <summary>
            /// Список должностей.
            /// </summary>
            public List<String> Dolzn {
                get { return this._dolzn; }
                set { this._dolzn = value; }
            }

            /// <summary>
            /// Список отделов.
            /// </summary>
            public List<String> Otdels
            {
                get { return this._otdels; }
                set { this._otdels = value; }
            }

        }

        /// <summary>
        /// Инициализирует класс <see cref="CListsDep"/>
        /// </summary>
        public organizacia()
        {
            this._list_dolzn_otdels = new CListsDep();

        }

        /// <summary>
        /// Сохраняет класс в файл. 
        /// Вернее сохраняет списки в файл.
        /// </summary>
        /// <param name="from_file">Путь к файлу.</param>
        public void save(String from_file)
        {
            savii.save_calss(from_file, this._list_dolzn_otdels);
        }

        /// <summary>
        /// Открывает из файла списки.
        /// </summary>
        /// <param name="from_file">Путь к файлу</param>
        public void open(String from_file)
        {
            this._list_dolzn_otdels = (CListsDep)savii.open_class(from_file, typeof(rab_list));
        }

        public CListsDep ListDolznOtdels
        {
            get { return this._list_dolzn_otdels; }
            set { this._list_dolzn_otdels = value; }
        }
    }




    //public class rab_list
    //{
    //    public List<rab> LS_rab;
    //    public rab_list()
    //    {
    //        this.LS_rab = new List<rab>();
    //    }
    //}


    /// <summary>
    /// Класс хранит информацию  о работнике. 
    /// </summary>
    public class rab
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        private String _Familia;
        private String _name;
        private String _otchestvo;
        private String _TRANSLIT;
        private bool _Naxalnik_otd_da_net = false;
        // kto_podpis;
        private int _dolznost = 0, _otdel = 0;
        private CTimeС _DT_rabochi_den;
        public rab()
        {

        }
        public rab(String Familia, String name, String otchestvo, String TRANSLIT, CTimeС DT_rabochi_den, int dolzn, int otdel, bool Naxalnik_otd_da_net)
        {
            this._Familia = Familia;
            this._name = name;
            this._otchestvo = otchestvo;
            this._TRANSLIT = TRANSLIT;
            this._DT_rabochi_den = DT_rabochi_den;
            this._dolznost = dolzn;
            this._otdel = otdel;
            this._Naxalnik_otd_da_net = Naxalnik_otd_da_net;
        }
        public static string obj_(List_obj tes, int i)
        {
            if (i > -1)
            {
                return tes[i].OBJ;
            }
            return null;
        }
     
        /// <summary>
        /// Фамилия
        /// </summary>
        public String Familia
        {
            get { return this._Familia; }
            set { this._Familia = value; }
        }

        public String Name
        {
            get { return this._name; }
            set { this._name = value; }
        }
        public String Otchestvo
        {
            get { return this._otchestvo; }
            set { this._otchestvo = value; }
        }
        public String TRANSLIT
        {
            get { return this._TRANSLIT; }
            set { this._TRANSLIT = value; }
        }
        public bool isDepartmentBoss
        {
            get { return this._Naxalnik_otd_da_net; }
            set { this._Naxalnik_otd_da_net = value; }
        }
        public int Dolznost
        {
            get { return this._dolznost; }
            set { this._dolznost = value; }
        }
        public int Otdel
        {
            get { return this._otdel; }
            set { this._otdel = value; }
        }

        public CTimeС DT_rabochi_den
        {
            get { return this._DT_rabochi_den; }
            set { this._DT_rabochi_den = value; }
        }

    }
    public class DATATest_otd
    {
        public string ID { get; set; }
        public string OBJ { get; set; }
    }
    public class Test
    {
        public string Data_in_month { get; set; }
        public string Begin_day { get; set; }
        public string End_of_day { get; set; }
        public string Hours_worked { get; set; }
    }
    public class Test1
    {

        public string Data_name { get; set; }
        public string Data_familia { get; set; }
        public string Data_otchestvo { get; set; }
        public string Data_FIO_transl { get; set; }
        public string Data_dolj { get; set; }
        public string Data_otd { get; set; }
        public string Data_norm { get; set; }
        public string Naxalnik_otd_da_net { get; set; }


        //    public int ID__ { get; set; }
        public Test1(rab LS_rab)
        {
            this.Data_name = LS_rab.Name;
            this.Data_familia = LS_rab.Familia;
            this.Data_otchestvo = LS_rab.Otchestvo;
            this.Data_FIO_transl = LS_rab.TRANSLIT;
            this.Data_dolj = rab.obj_((List_obj)savii.open_class("dolznost.xml", typeof(List_obj)), LS_rab.Dolznost);
            this.Data_otd = rab.obj_((List_obj)savii.open_class("otdeli.xml", typeof(List_obj)), LS_rab.Otdel);
            this.Data_norm = String.Format("{0}:{1}:{2}", LS_rab.DT_rabochi_den.Hour, LS_rab.DT_rabochi_den.Minute, LS_rab.DT_rabochi_den.Second);
            if (LS_rab.isDepartmentBoss == true)
            {
                this.Naxalnik_otd_da_net = "Да";
            }
            else
            {
                this.Naxalnik_otd_da_net = "Нет";
            }
        }
    }

    public class Object_i
    {

        public int ID = 0;
        public String OBJ = "";
        public Object_i(int ID, String OBJ)
        {
            this.ID = ID;
            this.OBJ = OBJ;
        }
        public Object_i()
        {

        }
    }

}
