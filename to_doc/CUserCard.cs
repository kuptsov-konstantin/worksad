﻿using KupcovKS.DataBase.AD;
using KupcovKS.ADFunction;
using KupcovKS.Logging;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
//using Json;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;



namespace KupcovKS.WordConverter
{
    using KupcovKS.Events;
    using Microsoft.Win32;
    using System.Windows.Forms;


    /// <summary>
    /// Пространство имен <see cref="WordConverter"/> слоужит для генерации карточки пользователя. И содержит следующие классы....
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }

    /*
     СДЕЛАТЬ ДЕРЕВО
     */

       

    /// <summary>
    ///Создание карточки пользователя по образцу в HTML файле. 
    /// </summary>
    public class CUserCard
    {
        #region Ивент Сообщение в основно поток
        public event EventHandler<MessageEventArgs> EMessage;
        #endregion

        #region Ивент Домен доступен
        public event EventHandler<AvailableDomainEventArgs> EAvailableDomain;
        #endregion


        /// <summary>
        /// Хранит связь с сервером домена.
        /// </summary>
        /// <seealso cref="System.DirectoryServices.AccountManagement.PrincipalContext"/>
        private PrincipalContext _ctx;

        private GroupPrincipal _grp;
        private List<string> _gruops;
        private CFirm _users;

        /// <summary>
        /// Хранит список пользователей  <see cref="CUserInfoInAD"/>
        /// </summary>
        /// <seealso cref="CUserInfoInAD"/>
        
        public CUserInfoInADBase UsersOnList
        {
            get;
            set;
        }
        /// <summary>
        /// Отделы
        /// </summary>
        private List<string> _departaments; 


        // Инициализирует класс. Конструктор во время инициализации пробует подключиться к AD через <see cref="init"/>.
        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public CUserCard() {
            CLog.Logging("CUser", CLog.file_def);
          //  this.init();
        }

        /// <summary>
        /// Деструктор. Отключается от сервера.
        /// </summary>
        ~CUserCard()
        {
            if (this._ctx != null) this._ctx.Dispose();
            if (this._grp != null) this._grp.Dispose();
        }

        /// <summary>
        /// Класс хранит пароль и СКД.
        /// Не используется.
        /// </summary>
        [DataContract]
        public class Person
        {
            /// <summary>
            /// Пароль.
            /// </summary>
            [DataMember]
            public string PAS;
            /// <summary>
            /// СКД код.
            /// </summary>
            [DataMember]
            public int SKD;

            /// <summary>
            /// Инициализирует класс.
            /// </summary>
            public Person() { }

            /// <summary>
            /// Инициализирует класс с параметрами пароля и скд.
            /// </summary>
            /// <param name="PAS">Пароль</param>
            /// <param name="SKD">СКД</param>
            public Person(string PAS, int SKD)
            {
                this.SKD = SKD;
                this.PAS = PAS;
            }
        }
        string new_HTMLpage;

        public class CReturn
        {
            public string Puth { get; set; }
            public List<string> TextAreas { get; set; }
           // public string FIO { get; set; }
        }
        
        /// <summary>
        /// Формирование документа MS Word по HTML фалу. Через входной параметр задается код(sid) из AD.
        /// </summary>
        /// <param name="SID">Параметр AD, хранящийся в базе AD и представляющий кодовый адресс записи.</param>
        /// <returns> Путь к файлу</returns>
        public CReturn GetHTMLTemplate(string SID, bool TextArea)
        {
            try
            {
                var CurrDir = Directory.GetCurrentDirectory();
                var filepath = File.OpenText(CurrDir + "\\HTMLPage1.html");
                string document = filepath.ReadToEnd();
                List<string> rez = new List<string>();
                List<string> rez_textbox = new List<string>();

                string text = document;
              
                string strRegex = @"(?<=([%]))(\w*)(?=([%]))";
                string strRegex_textBox = @"(?<=([$]))(\w*)(?=([$]))";
                Regex myRegex = new Regex(strRegex, RegexOptions.None);
                foreach (Match myMatch in myRegex.Matches(text))
                {
                    if (myMatch.Success)
                    {
                        rez.Add(myMatch.Value);
                    }
                }
                rez = rez.Distinct().ToList();
                var ctx = new PrincipalContext(ContextType.Domain, Environment.UserDomainName);
                UserPrincipal foundUser = UserPrincipal.FindByIdentity(ctx, IdentityType.Sid, SID);/// поиск пользователя
                var e1 = (DirectoryEntry)foundUser.GetUnderlyingObject();//получение информации о сотруднике
              
                
                
                foreach (string str in rez)
                {
                    var ttee = AD.GetParamFromAD(e1, str) as string;
                    document = document.Replace('%' + str + '%', ttee);

                }
                var login = AD.GetParamFromAD(e1, "samAccountName") as string;

                var new_puth = CurrDir + "\\" + login + ".html";
                new_HTMLpage = document;


                CReturn Creturn = new CReturn();
             
                if (TextArea == true)
                {
                    myRegex = new Regex(strRegex_textBox, RegexOptions.None);
                    foreach (Match myMatch in myRegex.Matches(text))
                    {
                        if (myMatch.Success)
                        {
                            rez_textbox.Add(myMatch.Value);
                        }
                    }
                    rez_textbox = rez_textbox.Distinct().ToList();
                    Creturn.TextAreas = new List<string>();
                    foreach (var item in rez_textbox)
                    {
                        Creturn.TextAreas.Add(item);
                        var ff = "<input type=\"text\" size=\"40\">";
                        var TextBox_html = "<textarea name=\"myTextBox\" cols=\"40\" rows=\"5\"></textarea>";
                        document = document.Replace('$' + item + '$', String.Format("<textarea id=\"{0}\" name=\"{0}\" cols=\"20\" rows=\"1\"></textarea>", item));
                    }
                }



                File.WriteAllText(new_puth, document);






                Creturn.Puth = new_puth;

              //  Object oMissing = System.Reflection.Missing.Value;
                return Creturn;

                //if (_word == null) _word = new Word.Application();
              //  _word.Visible = true;
               // _wordDoc = _word.Documents.Open(strtty + "\\" + login + ".html");
               // File.Delete(strtty + "\\" + login + ".html");
            }
            catch (Exception trt)
            {
                return null;
            }
        }


        /// <summary>
        /// Формирование документа MS Word по HTML фалу. Через входной параметр задается код(sid) из AD.
        /// </summary>
        /// <param name="SID">Параметр AD, хранящийся в базе AD и представляющий кодовый адресс записи.</param>
        /// <returns> Путь к файлу</returns>
        public CReturn GetHTMLTemplate(string SID, bool TextArea, string shablon_path)
        {
            try
            {
                var CurrDir = Directory.GetCurrentDirectory();
                var filepath = File.OpenText(shablon_path);
                String document = filepath.ReadToEnd();
                List<string> rez = new List<string>();
                List<string> rez_textbox = new List<string>();

                string text = document;

                string strRegex = @"(?<=([%]))(\w*)(?=([%]))";
                string strRegex_textBox = @"(?<=([$]))(\w*)(?=([$]))";
                Regex myRegex = new Regex(strRegex, RegexOptions.None);
                foreach (Match myMatch in myRegex.Matches(text))
                {
                    if (myMatch.Success)
                    {
                        rez.Add(myMatch.Value);
                    }
                }
                rez = rez.Distinct().ToList();
                var ctx = new PrincipalContext(ContextType.Domain, Environment.UserDomainName);
                UserPrincipal foundUser = UserPrincipal.FindByIdentity(ctx, IdentityType.Sid, SID);/// поиск пользователя
                var e1 = (DirectoryEntry)foundUser.GetUnderlyingObject();//получение информации о сотруднике



                foreach (string str in rez)
                {
                    var ttee = AD.GetParamFromAD(e1, str) as String;
                    document = document.Replace('%' + str + '%', ttee);

                }
                var login = AD.GetParamFromAD(e1, "samAccountName") as String;

                var new_puth = CurrDir + "\\" + login + ".html";
                new_HTMLpage = document;


                CReturn Creturn = new CReturn();

                if (TextArea == true)
                {
                    myRegex = new Regex(strRegex_textBox, RegexOptions.None);
                    foreach (Match myMatch in myRegex.Matches(text))
                    {
                        if (myMatch.Success)
                        {
                            rez_textbox.Add(myMatch.Value);
                        }
                    }
                    rez_textbox = rez_textbox.Distinct().ToList();
                    Creturn.TextAreas = new List<string>();
                    foreach (var item in rez_textbox)
                    {
                        Creturn.TextAreas.Add(item);
                        var ff = "<input type=\"text\" size=\"40\">";
                        var TextBox_html = "<textarea name=\"myTextBox\" cols=\"40\" rows=\"5\"></textarea>";
                        document = document.Replace('$' + item + '$', String.Format("<textarea id=\"{0}\" name=\"{0}\" cols=\"20\" rows=\"1\"></textarea>", item));
                    }
                }



                File.WriteAllText(new_puth, document);






                Creturn.Puth = new_puth;

                //  Object oMissing = System.Reflection.Missing.Value;
                return Creturn;

                //if (_word == null) _word = new Word.Application();
                //  _word.Visible = true;
                // _wordDoc = _word.Documents.Open(strtty + "\\" + login + ".html");
                // File.Delete(strtty + "\\" + login + ".html");
            }
            catch (Exception trt)
            {
                return null;
            }
        }  

        public string UpdateTextAreasInHTML(CReturn Creturn)
        {
            try
            {
                string asdf = Path.GetExtension(Creturn.Puth);
                var new_puth = Creturn.Puth;
                new_puth = new_puth.Replace(asdf, "_" + asdf);
                if (Creturn.TextAreas.Count > 0)
                {
                    CLog.Logging("Содержит элеметнты в " + Convert.ToString(Creturn.TextAreas.Count), Creturn.TextAreas.GetType());
                }
                else
                {
                    CLog.Logging("Не содержатся элеметнты в ", Creturn.TextAreas.GetType());

                }
                var file = File.ReadAllText(Creturn.Puth);
                foreach (var item in Creturn.TextAreas)
                {
                    file = file.Replace(string.Format("<textarea id=\"{0}\" name=\"{0}\" cols=\"20\" rows=\"1\"></textarea>", item), "_");
                }
                File.WriteAllText(new_puth, file);        
                return new_puth;
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Не могу найти MS Word!");
                CLog.Logging(ex.Message);
                return string.Empty;
            }
        }

      /*  void _word_WindowActivate(Word.Document Doc, Word.Window Wn)
        {
            CLog.Logging("Активен ворд");
            //throw new NotImplementedException();
        }*/
        /// <summary>
        /// Формирование документа MS Word по HTML фалу. Через входящие параметры задается фио, логин, пароль, скд и Email.
        /// </summary>
        /// <param name="FIO">ФИО сотрудника</param>
        /// <param name="login">Логин сотрудника</param>
        /// <param name="pass">Пароль сотрудника</param>
        /// <param name="SKD">СКД</param>
        /// <param name="post">E-mail</param>
        [Obsolete]
        public string HTML_to_doc(string FIO, string login, string pass, string SKD, string post)
        {          
            try
            {
                var strtty = Directory.GetCurrentDirectory();
                var filepath = File.OpenText(strtty + "\\HTMLPage1.html");
                String tesvt = filepath.ReadToEnd();
                List<string> rez = new List<string>();
                string text = tesvt;

                string strRegex = @"(?<=([%]))(\w*)(?=([%]))";
                Regex myRegex = new Regex(strRegex, RegexOptions.None);
                //    string strTargetString = @"Regex Hero is a real-time online Silverlight %Regular% Expression %Tester%.";

                foreach (Match myMatch in myRegex.Matches(text))
                {
                    if (myMatch.Success)
                    {
                        rez.Add(myMatch.Value);
                    }
                }
                rez = rez.Distinct().ToList();



                foreach (string str in rez)
                {


                }
                String[] tem_z = { "$FIO", "$login", "$pass", "$skd", "$mail" };

                /* GetParamFromAD(e, "sAMAccountName");
                             GetParamFromAD(e, "cn");
                             GetParamFromAD(e, "mail");
                             GetParamFromAD(e, "manager");
                 */

                tesvt = tesvt.Replace(tem_z[0], FIO);
                tesvt = tesvt.Replace(tem_z[1], login);
                tesvt = tesvt.Replace(tem_z[2], pass);
                tesvt = tesvt.Replace(tem_z[3], SKD);
                tesvt = tesvt.Replace(tem_z[4], post);
                string new_file = strtty + "\\temp.html";
                File.WriteAllText(new_file, tesvt);
                return new_file;
              /*  Object oMissing = System.Reflection.Missing.Value;
                if (_word == null) _word = new Word.Application();
                _word.Visible = true;
                _wordDoc = _word.Documents.Open(strtty + "\\temp.html");*/
            }
            catch (Exception trt)
            {
                return string.Empty;
            }
        }

        /*инициализация связи с доменом*/


        /// <summary>
        /// Инициализация подключения к AD.
        /// </summary>
        /// <seealso cref="System.DirectoryServices.AccountManagement.PrincipalContext"/>
        public void Initialize()
        {
            try
            {
                this._ctx = new PrincipalContext(ContextType.Domain, Environment.UserDomainName);
                CLog.Logging("init CUser");
                EAvailableDomain(this, new AvailableDomainEventArgs() { isDostup = true, Domain = Environment.UserDomainName });
            }
            catch (Exception e)
            {

                this._ctx = null;
                CLog.Logging(e.Message, this.GetType(), CLog.file_error);
                EMessage(this, new MessageEventArgs() { Message = "Нет доступа к домену!!!" + Environment.NewLine + e.Message, isExit = true });
            }
        }


        public string LoggedUserName
        {
            get
            {
                try
                {
                    return UserPrincipal.Current.DisplayName;
                }
                catch (Exception ex)
                {
                    CLog.Logging(ex);
                    return "";
                }
            }         
        }
        /// <summary>
        /// Получение информации о сотруднике из AD, по имени и фамилии.
        /// </summary>
        /// <param name="firstname">Имя</param>
        /// <param name="lastname">Фамилия</param>
        /// <returns>Возвращает инициализированный класс который хранит информацию о сотруднике <see cref="CWorkerInfoAD"/></returns>
        public static CWorkerInfoAD GetWorkerInfo(string firstname, string lastname)
        {
            if (firstname != null)
            {
                if (lastname != null)
                {
                    string DomainPath = GetDomainFullName(Environment.UserDomainName);
                    //DirectoryEntry searchRoot = ;
                    DirectorySearcher DS = new DirectorySearcher(new DirectoryEntry("LDAP://" + DomainPath), string.Format("(&(objectCategory=person)(objectClass=user)(givenname={0})(sn={1}))", firstname, lastname), new String[] { "name", "manager" });
                  //  DirectorySearcher d = new DirectorySearcher(searchRoot);
                  //  d.Filter = string.Format("(&(objectCategory=person)(objectClass=user)(givenname={0})(sn={1}))", firstname, lastname);
                  //  d.PropertiesToLoad.Add("name");
                   // d.PropertiesToLoad.Add("manager");//начальник
                    var resultCol = DS.FindAll();
                    SearchResult result;
                    if (resultCol != null)
                    {
                        for (int counter = 0; counter < resultCol.Count; counter++)
                        {
                            string UserNameEmailString = string.Empty;
                            result = resultCol[counter];
                            if (result != null)
                            {
                                string FIO = null;
                                //AD.GetParamFromAD();
                                if (result.Properties.Contains("name") == true)
                                {
                                    FIO = (string)result.Properties["name"][0];
                                }
                                else
                                {
                                    FIO = "";
                                }


                                if (result.Properties.Contains("manager") == true)
                                {
                                    var manager = (string)result.Properties["manager"][0];
                                    UserPrincipal foundUser1 = UserPrincipal.FindByIdentity(new PrincipalContext(ContextType.Domain, Environment.UserDomainName), manager);
                                    var e11 = (DirectoryEntry)foundUser1.GetUnderlyingObject();
                                    var FIO_n = AD.GetParamFromAD(e11, "cn") as String;
                                    return new CWorkerInfoAD(FIO, FIO_n) ;

                                }
                                else
                                {
                                    return new CWorkerInfoAD(FIO, "-- -- --");

                                }

                            }
                        }
                    }
                }
            }
            return null;
        }

        /*Получение списка пользователей из группы*/

        /// <summary>
        /// Получает список пользователей из группы по ID группы.
        /// </summary>
        /// <param name="grups_id">Идентификатор гпуппы</param>
        /// <returns>Возвращает инициализированный класс который хранит информацию о сотруднике <see cref="CWorkerInfoAD"/></returns>
        public List<CWorkerInfoAD> GetUserList(int grups_id)
        {
            if (this._gruops == null) return null;
            this._grp = GroupPrincipal.FindByIdentity(this._ctx, IdentityType.SamAccountName, this._gruops[grups_id]);
            this._users = new CFirm();
            if (_grp != null)
            {
                var tt = _grp.GetMembers(true);
                foreach (Principal p in tt)
                {
                    var e = p.GetUnderlyingObject() as DirectoryEntry;
                    this._users.Add(e);
                }
            }
            return this._users;
        }

        /// <summary>
        /// Получение полного имени домена в который входит компьютер.
        /// </summary>
        /// <param name="friendlyName">Имя</param>
        /// <returns>Полное имя</returns>
        public static string GetDomainFullName(string friendlyName)
        {
            DirectoryContext context = new DirectoryContext(DirectoryContextType.Domain, friendlyName);
            Domain domain = Domain.GetDomain(context);
            return domain.Name;
        }

        /*НАВЕРНО ТУТ БУДУ ИСКАТЬ ЧЕЛА ПО SID*/
        /*Получение всех пользователей*/

        /// <summary>
        /// Получение информации о пользователи по SID из AD.
        /// </summary>
        /// <param name="SID">SID сотрудника</param>
        /// <returns>Возвращает инициализированный класс который хранит информацию о сотруднике <see cref="CWorkerInfoAD"/></returns>
        public CWorkerInfoAD GetUSERbySID(string SID)
        {
            
            var ctx = new PrincipalContext(ContextType.Domain, Environment.UserDomainName);
            UserPrincipal foundUser = UserPrincipal.FindByIdentity(ctx, IdentityType.Sid, SID);// поиск пользователя
            var e1 = foundUser.GetUnderlyingObject() as DirectoryEntry;//получение информации о сотруднике
            string FIO, mail, login, department, manager, FIO_n;
            if ((e1.Properties["manager"]).Value != null)
            {
                manager = (e1.Properties["manager"]).Value.ToString();
                UserPrincipal foundUser1 = UserPrincipal.FindByIdentity(ctx, manager);
                var e11 = foundUser1.GetUnderlyingObject() as DirectoryEntry;
                FIO_n = AD.GetParamFromAD(e11, "cn") as string;
                return new CWorkerInfoAD(AD.GetParamFromAD(e1, "sAMAccountName") as string, AD.GetParamFromAD(e1, "cn") as string, AD.GetParamFromAD(e1, "mail") as string, FIO_n);
            }
            else
            {
                return new CWorkerInfoAD(AD.GetParamFromAD(e1, "sAMAccountName") as string, AD.GetParamFromAD(e1, "cn") as string, AD.GetParamFromAD(e1, "mail") as string, "----");
            }



        }




        /*получение списка отделов... теперь!*/




        /*групп*/


        /// <summary>
        /// Получение списка групп.
        /// </summary>
        /// <returns>Список групп.</returns>
        public List<string> GetGruopList()
        {
            List<String> hhf = new List<String>();
            // var t = Environment.UserDomainName;
            if (this._gruops == null)
            {
                using (var searcher = new PrincipalSearcher(new GroupPrincipal(this._ctx)))
                {
                    foreach (var result in searcher.FindAll())
                    {
                        hhf.Add(result.Name);
                    }
                }
                this._gruops = hhf;
            }
            return this._gruops;
        }

        void GetUserInfo(int gruop, int nomer)
        {
            this._grp = GroupPrincipal.FindByIdentity(this._ctx, IdentityType.SamAccountName, "employees");
            int yy = 0;
          //  var tt = ;
            foreach (Principal p in this._grp.GetMembers(true))
            {
                if (yy == Convert.ToInt32(nomer))
                {
                    using (var DE = (DirectoryEntry)p.GetUnderlyingObject())
                    {
                        string FIO = (DE.Properties["cn"]).Value.ToString();
                        string mail = (DE.Properties["mail"]).Value.ToString();
                        string desc;
                        if ((DE.Properties["description"]) != null)
                        {
                            desc = (DE.Properties["description"]).Value.ToString();
                        }
                        else
                        {
                            desc = " ";
                        }
                        string login = (DE.Properties["sAMAccountName"]).Value.ToString();
                        if (desc != null)
                        {
                            Person p2 = new JavaScriptSerializer().Deserialize<Person>(desc.ToString());
                           // Console.Write("{0} {1} {2}", desc.ToString(), p2.PAS, p2.SKD);
                          string puth =  HTML_to_doc(FIO, login, p2.PAS, p2.SKD.ToString(), mail);

                            // button1_Click(FIO, login, p2.PAS, p2.SKD.ToString(), mail);

                        }
                    }
                }
                yy++;
            }
        }


      
        public List<string> GetAllDepartment(CUserInfoInADBase CUIIADB)
        {
            List<string> ls = new List<string>();
            if (CUIIADB == null) return null;
            for (int i = 0; i < CUIIADB.Count; i++)
            {
                bool add_ = true;
                for (int y = 0; y < ls.Count; y++)
                {
                    if (ls[y].CompareTo(CUIIADB[i]) == 0)
                    {
                        add_ = false;
                    }
                }
                if (add_ == true)
                {
                    ls.Add(CUIIADB[i].DEPARTMENT);
                }
            }
            return ls;
        }

        /// <summary>
        /// Получение списка отделов.
        /// </summary>
        /// <returns>Возвращается список отделов.</returns>
        public List<String> GetAllDepartment()
        {
            this._departaments = new List<String>();
            DirectoryEntry searchRoot = new DirectoryEntry("LDAP://" + GetDomainFullName(Environment.UserDomainName));
            DirectorySearcher search = new DirectorySearcher(searchRoot,
                "(objectClass=user)",
                new String[] { "department" }
                );
            using (SearchResultCollection resultCol = search.FindAll())
            {
                foreach (SearchResult result in resultCol)
                {
                    string dept = String.Empty;
                    DirectoryEntry de = result.GetDirectoryEntry();
                    if (de.Properties.Contains("department"))
                    {
                        dept = de.Properties["department"][0].ToString();
                        if (!this._departaments.Contains(dept))
                        {
                            this._departaments.Add(result.Properties["department"][0].ToString());
                        }
                    }
                }
            }           
            return this._departaments;
        }

        CUserInfoInADBase Users = null;

        /// <summary>
        /// Получение списка всех пользователей.
        /// </summary>
        /// <returns>Список пользователей.</returns>
        /// <seealso cref="CUserInfoInAD"/>
        public CUserInfoInADBase GetAllUsers()
        {
            try
            {
                if (Users == null)
                {
                    DirectoryEntry searchRoot = new DirectoryEntry("LDAP://" + GetDomainFullName(Environment.UserDomainName));
                    DirectorySearcher search = new DirectorySearcher(searchRoot,
                        "(&(objectClass=user)(objectCategory=person))",
                        new String[] { "cn", "objectsid", "department", "whenCreated" }
                       );
                    using (SearchResultCollection resultCol = search.FindAll())
                    {
                        if (resultCol != null)
                        {
                            CLog.Logging("GetALLUsers: " + Convert.ToString(resultCol.Count));
                            CUserInfoInADBase lstADUsers = new CUserInfoInADBase(resultCol);
                            Users = lstADUsers;
                            return lstADUsers;
                        }
                        else
                        {
                            CLog.Logging("Не получены данные");
                            System.Windows.Forms.MessageBox.Show("Нет данных");
                        }
                    }
                }
                else
                {
                    return Users;
                }               
            }
            catch (Exception ex)
            {
                CLog.Logging(ex);
            }
            return null;
        }
    }
}