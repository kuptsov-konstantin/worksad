﻿using KupcovKS.Events;
using KupcovKS.WordConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KupcovKS.RogerConverter
{
    /// <summary>
    /// Логика взаимодействия для FindDC.xaml
    /// </summary>
    public partial class FindDC : Window
    {
        CUserCard asdf;
        Thread TH2, TH1;
        #region Делегат закрытия окна.
       // public delegate void DCloseWindow();
        //   public DCloseWindow CloseWindow;
        /// <summary>
        /// Метод закрытия окна.
        /// </summary>
        public void MCloseWindow()
        {
            this.Dispatcher.Invoke(()=>this.Close());
        }
        #endregion

        public FindDC()
        {
            InitializeComponent();
            this.asdf = new CUserCard();
            this.asdf.EMessage += asdf_Message;
            this.asdf.EAvailableDomain  += asdf_EDomainIsDostupen;
            this.TH2 = new Thread(
                (obj) => {
                //var cuc = obj as CUserCard;
                (obj as CUserCard).Initialize();
            });
            this.TH2.Start(this.asdf);


        }   
        private void image1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }
        void asdf_Message(object sender, MessageEventArgs e)
        {
            if (e.isExit == true)
            {
                MessageBox.Show(e.Message);
                MCloseWindow();
                //label2.Text = arg.ToString();
                this.Close();

            }
            else
            {
                MessageBox.Show(e.Message);
            }
        }
        void asdf_EDomainIsDostupen(object sender, AvailableDomainEventArgs e)
        {
            if (e.isDostup == true)
            {
                Thread thread = new Thread(() =>
                {
                    MainWindow w = new MainWindow(this.asdf);
                    w.Show();

                });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
        }

    }
}
