﻿using KupcovKS.CSVInterpritation;
using KupcovKS.DataBase.AD;
using KupcovKS.Events;
using KupcovKS.Office;
using KupcovKS.WordConverter;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace KupcovKS.RogerConverter
{


    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Переменные
        hod_ HOD;
        List<CUserInfoInAD> List_USERS_in_gruop;        
        public string file_name;
        int CURRENT = 0, MAXIMUM = 0; 
        Thread TH1, TH2;
        private CUserCard cUserCard;

        #endregion
/*
        #region Делегат закрытия окна.
        public delegate void DCloseWindow();
        //   public DCloseWindow CloseWindow;
        /// <summary>
        /// Метод закрытия окна.
        /// </summary>
        public void MCloseWindow()
        {
            this.Close();
        }
        #endregion
*/ 
/*
        #region Делегат активаци кнопок.
        public delegate void DActivateButton();
       // public DActivateButton CloseWindow;
     
        /// <summary>
        /// Метод закрытия окна.
        /// </summary>
        void MActivateButton()
        {
            this.button1_obzor.IsEnabled = false;
            this.button3_obrabotka.IsEnabled = false;
            this.STOP_.IsEnabled = false;
        }

        #endregion

        */
        public MainWindow()
        {
            InitializeComponent();
            this.button1_obzor.IsEnabled = false;
            this.button3_obrabotka.IsEnabled = false;
            this.STOP_.IsEnabled = false;

        }

        public MainWindow(CUserCard cUserCard)
        {
            this.cUserCard = cUserCard;
            
            InitializeComponent();
            this.button1_obzor.IsEnabled = false;
            this.button3_obrabotka.IsEnabled = false;
            this.STOP_.IsEnabled = false;
            
        }

        void EX_EProgressI(object sender, ProgressIEventArgs e)
        {
            this.button3_obrabotka.Dispatcher.Invoke(() => { this.button3_obrabotka.IsEnabled = false; });
            progress__.Dispatcher.Invoke(() => { progress__.Maximum = e.Maximum; progress__.Value = e.Current; });
            this.button3_obrabotka.Dispatcher.Invoke(() =>
            {
                if (progress__.Maximum == progress__.Value)
                {
                    this.button3_obrabotka.IsEnabled = true;
                }
            });

        }

      
       /* void asdf_EDomainIsDostupen(bool isDostup)
        {
            if (!Dispatcher.CheckAccess()) // CheckAccess returns true if you're on the dispatcher thread
            {
                Dispatcher.Invoke(new DActivateButton(MActivateButton));
                return;
            }
            MActivateButton();
        }*/
       /* void init_DT()
        {
            this.DT_hod = new DispatcherTimer();
            this.DT_hod.Interval = new TimeSpan(1);
            this.DT_hod.Tick += DT_hod_Tick;
        }*/
   

        /*

        void asdf_Message(string str, bool isExit)
        {
            if (isExit == true)
            {
                MessageBox.Show(str);
                if (!Dispatcher.CheckAccess()) // CheckAccess returns true if you're on the dispatcher thread
                {
                    Dispatcher.Invoke(new DCloseWindow(MCloseWindow));
                    return;
                }
              //label2.Text = arg.ToString();
                this.Close();

            }
            else
            {
                MessageBox.Show(str);
            }
        }

        void Tomer_for_hod_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (this.HOD != null)
            {
                this.HOD.Setup_param(this.CURRENT, this.MAXIMUM);
            }
            else
            {
                // this.HOD = new hod_();
                // this.HOD.Show();
            }
        }

        private void TTC(object state)
        {
            if (this.HOD != null)
            {
                this.HOD.Setup_param(this.CURRENT, this.MAXIMUM);
            }
            else
            {
                this.HOD = new hod_();
                this.HOD.Show();
            }
        }

        void DT_hod_Tick(object sender, EventArgs e)
        {
            if (this.HOD != null)
            {
                this.HOD.Setup_param(this.CURRENT, this.MAXIMUM);
            }
            else
            {
                this.HOD = new hod_();
                this.HOD.Show();
            }
        }*/

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            // OFD.ShowDialog();
            var BL = (bool)OFD.ShowDialog();
            if (BL == true)
            {
                this.button3_obrabotka.IsEnabled = true;
                this.file_name = OFD.FileName;
                PUT1.Content = OFD.SafeFileName;

            }
            else
            {
                this.file_name = "";
            }

        }
        /*
        void Init_hod_window()
        {
            this.HOD = new hod_();
            HOD.Show();
        }
        */
      
       /*
        void INIT_HOD()
        {
            this.TH = new Thread(Init_hod_window);
            this.TH.SetApartmentState(ApartmentState.STA);
            this.TH.Start();
        }*/

        /*пользователи*/
       /* private void namess_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }*/
       /*
        object GetParam(PrincipalContext ctx, string StrokaPodkluch, string poluchaemoe)
        {
            UserPrincipal foundUser1 = UserPrincipal.FindByIdentity(ctx, StrokaPodkluch);
            string temp = null;
            var e11 = (DirectoryEntry)foundUser1.GetUnderlyingObject();
            if ((e11.Properties[poluchaemoe]).Value != null)
                temp = (e11.Properties[poluchaemoe]).Value.ToString();
            else
                temp = "";
            return temp;
        }*/

        /*
         * задумка
         * хранить две строки - CN человека
         * название отдела (врятли.. но)
         * начальника CN
         */
        /*
        string test_obs(DirectoryEntry e1, string dan)
        {
            if (dan.CompareTo("objectsid") == 0)
            {
                var sidInBytes = (byte[])(e1.Properties[dan]).Value;
                var sid = new SecurityIdentifier(sidInBytes, 0);
                // This gives you what you want
                return sid.ToString();
            }
            else
                if ((e1.Properties[dan]).Value != null)
                    return (e1.Properties[dan]).Value.ToString();
                else
                    return "";
        }

        */
        /*
        void funct2(string firstname, string lastname)
        {
            string DomainPath = to_doc.CUserCard.GetDomainFullName(Environment.UserDomainName);
            DirectoryEntry searchRoot = new DirectoryEntry("LDAP://" + DomainPath);
            DirectorySearcher d = new DirectorySearcher(searchRoot);
            d.Filter = string.Format("(&(objectCategory=person)(objectClass=user)(givenname={0})(sn={1}))", firstname, lastname);
            d.PropertiesToLoad.Add("name");
            d.PropertiesToLoad.Add("cn");
            d.PropertiesToLoad.Add("sn");
            d.PropertiesToLoad.Add("manager");
            var result = d.FindAll();


        }
        */
 
        private void button3_Click(object sender, RoutedEventArgs e)
        {
           

            if (this.file_name != null)
            {
                //button3.IsEnabled = false;
                this.TH1 = new Thread(to_excel_thread);
                this.TH1.SetApartmentState(ApartmentState.STA);
                this.TH1.Start(null);
                // TH.Join();
                //   button3.IsEnabled = true;
            }
        }

        /*
        private void DT_Tick(object sender, EventArgs e)
        {
            // this.HOD = new hod_();
            try
            {
                this.button3_obrabotka.IsEnabled = false;
                progress__.Maximum = this.EX.BD_c.List_of_users.Count;
                progress__.Value = this.EX.vot;
                if (progress__.Maximum == progress__.Value)
                {
                    this.button3_obrabotka.IsEnabled = true;
                }
            }
            catch (Exception e1)
            {

            }

            //throw new NotImplementedException();
        }*/

        private void to_excel_thread()
        {
            COffice office = new COffice();
            office.EProgressI += EX_EProgressI;
            office.CreateExcel(CWorkWithExcel.CsvToDB(this.file_name));
            this.button3_obrabotka.Dispatcher.Invoke(
                () =>
                {
                    this.button3_obrabotka.IsEnabled = true;
                });
        }

        private void image1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }
        //Выход
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {               
                this.TH1.Abort();
            }
            catch (Exception er)
            {
            }
            Application.Current.MainWindow.Close();
        }

        private void STOP__Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.TH1.Abort();        
                this.STOP_.IsEnabled = false;
                this.button3_obrabotka.IsEnabled = false;
            }
            catch (Exception er)
            {
            }
        }
      
    }
}
